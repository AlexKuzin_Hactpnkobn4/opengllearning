    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aNormal;
    layout (location = 1) in vec3 aPos;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    out vec3 normal;
    out vec3 fragPos;

    void main()
    {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        normal = aNormal;
        fragPos = vec3(model * vec4(aPos, 1.0));
    }

    #type fragment
    #version 330 core

    out vec4 color;

    struct Material {
        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
        float shininess;
    };

    struct Light {
        vec3 position;

        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
    };

    uniform Light light;

    uniform Material material;
    uniform vec3 objectColor;
    uniform vec3 viewPos;

    in vec3 normal;
    in vec3 fragPos;

    void main()
    {
        vec3 ambient = light.ambient * material.ambient;

        vec3 norm = normalize(normal);
        vec3 lightDir = normalize(light.position - fragPos);

        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuze = light.diffuse * (diff * material.diffuse);

        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = light.specular * (spec * material.specular);

        vec3 result = (ambient + diffuze + specular) * objectColor;
        color = vec4(result, 1.0);
    }
