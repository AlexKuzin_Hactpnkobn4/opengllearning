    #type vertex
    #version 330 core

    layout (location = 0) in vec2 aPos;
    layout (location = 1) in vec2 aTexCoord;

    out vec2 fTexCoord;

    void main()
    {
        gl_Position = vec4(aPos, 0.0, 1.0);
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec2 fTexCoord;

    uniform sampler2D texture1;

    out vec4 color;

    const float offset = 3.0 / 300.0;

    void main()
    {
        vec2 offsets[9] = vec2[](
        vec2(-offset,  offset), // верхний-левый
        vec2( 0.0f,    offset), // верхний-центральный
        vec2( offset,  offset), // верхний-правый
        vec2(-offset,  0.0f),   // центральный-левый
        vec2( 0.0f,    0.0f),   // центральный-центральный
        vec2( offset,  0.0f),   // центральный-правый
        vec2(-offset, -offset), // нижний-левый
        vec2( 0.0f,   -offset), // нижний-центральный
        vec2( offset, -offset)  // нижний-правый
        );

        float kernel[9] = float[](
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
        );

        vec3 sampleTex[9];
        for(int i = 0; i < 9; i++)
        {
            sampleTex[i] = vec3(texture(texture1, fTexCoord.st + offsets[i]));
        }
        vec3 col = vec3(0.0);
        for(int i = 0; i < 9; i++)
        col += sampleTex[i] * kernel[i];

        color = vec4(col, 1.0);
//        color = vec4( vec3( 1.0 - texture(texture1, fTexCoord)), 1.0);
    }
