    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aNormal;
    layout (location = 1) in vec3 aPos;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    out vec3 normal;
    out vec3 fragPos;

    void main()
    {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        normal = aNormal;
        fragPos = vec3(model * vec4(aPos, 1.0));
    }

    #type fragment
    #version 330 core

    out vec4 color;

    uniform vec3 objectColor;
    uniform vec3 lightColor;
    uniform vec3 lightPos;
    uniform vec3 viewPos;

    in vec3 normal;
    in vec3 fragPos;

    void main()
    {
        float ambientStrengh = 0.15;
        vec3 ambient = ambientStrengh * lightColor;

        vec3 norm = normalize(normal);
        vec3 lightDir = normalize(lightPos - fragPos);

        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuze =  diff * lightColor;

        float specularStrength = 0.4;
        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        vec3 specular = specularStrength * spec * lightColor;

        vec3 result = (ambient + diffuze + specular) * objectColor;
        color = vec4(result, 1.0);
    }
