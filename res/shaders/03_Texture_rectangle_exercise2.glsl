    #type vertex
    #version 330 core

    layout (location = 0) in vec2 aPos;
    layout (location = 1) in vec3 aColor;
    layout (location = 2) in vec2 aTexCoord;

    out vec3 fColor;
    out vec2 fTexCoord;

    void main()
    {
        gl_Position = vec4(aPos, 0.0, 1.0);
        fColor = aColor;
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec3 fColor;
    in vec2 fTexCoord;

    uniform sampler2D texture1;
    uniform sampler2D texture2;
    uniform float mixValue;

    out vec4 color;

    void main()
    {
        color = mix(texture(texture1, vec2(1-fTexCoord.x, fTexCoord.y)), texture(texture2, fTexCoord), mixValue );
    }
