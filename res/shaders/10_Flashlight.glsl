    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec3 aNormal;
    layout (location = 2) in vec2 aTexCoord;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    out vec3 normal;
    out vec3 fragPos;
    out vec2 texCoord;

    void main()
    {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        normal = mat3(model) *  aNormal;
        fragPos = vec3(model * vec4(aPos, 1.0));
        texCoord = aTexCoord;
    }

        #type fragment
        #version 330 core

    out vec4 color;

    struct Material {
        sampler2D diffuse;
        sampler2D specular;
        float shininess;
    };

    struct Light {
        vec3 position;
        vec3 direction;
        float cutoff;
        float outerCutoff;

        float constant;
        float linear;
        float quadratic;

        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
    };

    uniform Light light;
    uniform Material material;
    uniform vec3 viewPos;

    in vec3 normal;
    in vec3 fragPos;
    in vec2 texCoord;

    void main()
    {
        vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));

        vec3 norm = normalize(normal);
        vec3 lightDir = normalize(light.position - fragPos);
        float theta = dot(lightDir, normalize(-light.direction));

        float epsilon = light.cutoff - light.outerCutoff;
        float intencity = clamp((theta-light.outerCutoff)/epsilon, 0.0, 1.0);

        float distance = length(light.position - fragPos);
        float attenuation = 1.0/(light.constant + light.linear * distance + light.quadratic * (distance * distance));

        float diff = max(dot(norm, lightDir), 0.1);
        vec3 diffuze = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));

        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

        vec3 result = (ambient + diffuze*attenuation*intencity + specular*attenuation*intencity) ;
        color = vec4(result, 1.0);
    }
