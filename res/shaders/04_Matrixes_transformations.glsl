    #type vertex
    #version 330 core

    layout (location = 0) in vec2 aPos;
    layout (location = 1) in vec3 aColor;
    layout (location = 2) in vec2 aTexCoord;

    out vec3 fColor;
    out vec2 fTexCoord;

    uniform mat4 transMat;

    void main()
    {
        gl_Position = transMat * vec4(aPos, 0.0, 1.0);
        fColor = aColor;
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec3 fColor;
    in vec2 fTexCoord;

    uniform sampler2D texture1;

    out vec4 color;

    void main()
    {
        color = texture(texture1, fTexCoord);
    }
