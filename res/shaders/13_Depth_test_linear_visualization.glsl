    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec2 aTexCoord;

    out vec2 fTexCoord;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main()
    {
        gl_Position = projection* view * model * vec4(aPos, 1.0);
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec2 fTexCoord;

    uniform sampler2D texture1;

    out vec4 color;

    float near = 0.1;
    float far  = 100.0;

    float LinearizeDepth(float depth)
    {
        float z = depth * 2.0 - 1.0; // обратно к NDC
        return (2.0 * near * far) / (far + near - z * (far - near));
    }

    void main()
    {
        float depth = LinearizeDepth(gl_FragCoord.z) / far; // делим на far для наглядности
        color = vec4(vec3(depth), 1.0);
    }
