    #type vertex
    #version 330 core

    layout (location = 0) in vec2 aPos;
    layout (location = 1) in vec2 aTexCoord;

    out vec2 fTexCoord;

    void main()
    {
        gl_Position = vec4(aPos, 0.0, 1.0);
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec2 fTexCoord;

    uniform sampler2D texture1;

    out vec4 color;

    void main()
    {
        color = vec4( vec3( 1.0 - texture(texture1, fTexCoord)), 1.0);
    }
