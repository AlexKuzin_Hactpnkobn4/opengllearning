    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec2 aTexCoord;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main()
    {
        gl_Position = projection* view * model * vec4(aPos, 1.0);
    }

    #type fragment
    #version 330 core

    out vec4 color;

    void main()
    {
        color = vec4(0.04, 0.28, 0.26, 1.0);
    }
