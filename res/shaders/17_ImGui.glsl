    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec2 aTexCoord;
    out vec2 fTexCoord;

    layout (std140) uniform projectionAndView
    {
        mat4 projection;
        mat4 view;
    };
    uniform mat4 model;

    void main()
    {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        fTexCoord = aTexCoord;
    }

    #type fragment
    #version 330 core

    in vec2 fTexCoord;

    uniform sampler2D texture1;

    out vec4 color;

    void main()
    {
        color = texture(texture1, fTexCoord);
        if(color.a < 0.1) discard;
    }
