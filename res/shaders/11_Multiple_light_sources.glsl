    #type vertex
    #version 330 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec3 aNormal;
    layout (location = 2) in vec2 aTexCoord;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    out vec3 normal;
    out vec3 fragPos;
    out vec2 texCoord;

    void main()
    {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        normal = mat3(model) *  aNormal;
        fragPos = vec3(model * vec4(aPos, 1.0));
        texCoord = aTexCoord;
    }

    #type fragment
    #version 330 core



    struct Material {
        sampler2D diffuse;
        sampler2D specular;
        float shininess;
    };

    struct DirectionalLight {
        vec3 direction;
        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
    };

    struct FlashLight {
        vec3 position;
        vec3 direction;
        float cutoff;
        float outerCutoff;

        float constant;
        float linear;
        float quadratic;

        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
    };

    struct PointLight {
        vec3 position;

        float constant;
        float linear;
        float quadratic;

        vec3 ambient;
        vec3 diffuse;
        vec3 specular;
    };

    uniform DirectionalLight directionalLight;
    uniform FlashLight flashLight;
    #define NR_POINT_LIGHTS 4
    uniform PointLight pointLights[NR_POINT_LIGHTS];
    uniform Material material;
    uniform vec3 viewPos;

    vec3 CalcDirLight(DirectionalLight light, vec3 norm, vec3 viewDir);
    vec3 CalcPointLight(PointLight light, vec3 norm, vec3 fragPos, vec3 viewDir);
    vec3 CalcFlashLight(FlashLight light, vec3 norm, vec3 fragPos, vec3 viewDir);

    in vec3 normal;
    in vec3 fragPos;
    in vec2 texCoord;

    out vec4 color;

    void main()
    {

        vec3 norm = normalize(normal);
        vec3 viewDir = normalize(viewPos - fragPos);

        vec3 result = CalcDirLight(directionalLight, norm, viewDir);
        result += CalcFlashLight(flashLight, norm, fragPos, viewDir);
        for(int i = 0; i < NR_POINT_LIGHTS; i++){
            result += CalcPointLight(pointLights[i], norm, fragPos, viewDir);
        }

        color = vec4(result, 1.0);

    }

    vec3 CalcDirLight(DirectionalLight light, vec3 norm, vec3 viewDir){

        vec3 lightDir = normalize(light.direction );

        float diff = max(dot(norm, lightDir), 0.1);

        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

        vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));
        vec3 diffuze = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));
        vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

        return (ambient + diffuze + specular) ;
    }

    vec3 CalcPointLight(PointLight light, vec3 norm, vec3 fragPos, vec3 viewDir){
        vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));

        float distance = length(light.position - fragPos);
        float attenuation = 1.0/(light.constant + light.linear * distance + light.quadratic * (distance * distance));

        vec3 lightDir = normalize(light.position - fragPos);

        float diff = max(dot(norm, lightDir), 0.1);
        vec3 diffuze = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));

        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

        return (ambient*attenuation + diffuze*attenuation + specular*attenuation) ;
    }

    vec3 CalcFlashLight(FlashLight light, vec3 norm, vec3 fragPos, vec3 viewDir){
        vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));

        vec3 lightDir = normalize(light.position - fragPos);
        float theta = dot(lightDir, normalize(-light.direction));

        float epsilon = light.cutoff - light.outerCutoff;
        float intencity = clamp((theta-light.outerCutoff)/epsilon, 0.0, 1.0);

        float distance = length(light.position - fragPos);
        float attenuation = 1.0/(light.constant + light.linear * distance + light.quadratic * (distance * distance));

        float diff = max(dot(norm, lightDir), 0.1);
        vec3 diffuze = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));

        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

        return (ambient + diffuze*attenuation*intencity + specular*attenuation*intencity) ;
    }
