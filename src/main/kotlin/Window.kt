import imgui.ImGui
import imgui.flag.ImGuiConfigFlags
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL20.*
import org.lwjgl.system.MemoryUtil.NULL


object Window {
    var WIDTH = 750
    var HEIGHT = 512
    val window: Long

    init {
        GLFWErrorCallback.createPrint(System.err).set()
        if (!glfwInit())
            throw IllegalStateException("Unable to initialize GLFW")

        glfwDefaultWindowHints()
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2)
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE)
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)

        window = glfwCreateWindow(WIDTH, HEIGHT, "OpeGL Learning", NULL, NULL)
        if (window == NULL)
            throw RuntimeException("Failed to create the GLFW window")

        glfwSetKeyCallback(         window, KeyListener::keyCallback)
        glfwSetScrollCallback(      window, MouseListener::mouseScrollCallback)
        glfwSetCursorPosCallback(   window, MouseListener::mousePosCallback)
        glfwSetMouseButtonCallback( window, MouseListener::mouseButtonCallback)

        val vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor())
        glfwSetWindowPos(window, (vidmode!!.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 2)

        glfwMakeContextCurrent(window)
        glfwSwapInterval(0)

        // Make the window visible
        glfwShowWindow(window)
        GL.createCapabilities()

        GuiLayer.init()
    }

    fun run(init: () -> Unit, loop: () -> Unit){
        init.invoke()
        glClearColor(0.1f,0.11f,0.13f,1.0f)
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT or GL_STENCIL_BUFFER_BIT)
            loop.invoke()
            GuiLayer.draw()
            MouseListener.clear()
            KeyListener.clear()

            if (ImGui.getIO().hasConfigFlags(ImGuiConfigFlags.ViewportsEnable)) {
                val backupWindowPtr = glfwGetCurrentContext()
                ImGui.updatePlatformWindows()
                ImGui.renderPlatformWindowsDefault()
                glfwMakeContextCurrent(backupWindowPtr)
            }

            glfwSwapBuffers(window) // swap the color buffers
            glfwPollEvents()
        }
        GuiLayer.dispose()
    }
}