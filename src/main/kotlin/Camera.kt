import engine.shaders.UniformBuffer
import org.joml.Math
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW

abstract class Camera {
    abstract fun getViewMatrix(): Matrix4f
    abstract fun getProjectionMatrix(): Matrix4f

    abstract fun update()
}

class FpsCamera: Camera() {
    var mouseSensitivity = 0.1f
    var cameraSpeed = 0.0012f

    var cameraPos = Vector3f(0f,0f,10f)
    var cameraFront = Vector3f(0f,0f,-1f)
    val cameraUp = Vector3f(0f,1f,0f)

    val projection = Matrix4f().perspective(Math.toRadians(45f), 800f / 600f, 0.1f, 100f)

    private var yaw = -90.0
    private var pitch = 0.0
    private var cameraInitialized = false

    override fun getViewMatrix(): Matrix4f {
        return Matrix4f().lookAt(
            cameraPos,
            (cameraPos.clone() as Vector3f).add(cameraFront),
            cameraUp,
        )
    }

    override fun getProjectionMatrix(): Matrix4f {
        return projection
    }

    override fun update() {
        updateUniformBuffer()

        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_W)) {
            cameraPos = (cameraFront.clone() as Vector3f).mulAdd(cameraSpeed, cameraPos)
        }
        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_S)) {
            cameraPos = (cameraFront.clone() as Vector3f).mulAdd(-cameraSpeed, cameraPos)
        }
        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_A)) {
            cameraPos = (cameraFront.clone() as Vector3f).cross(cameraUp).normalize().mulAdd(-cameraSpeed, cameraPos)
        }
        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_D)) {
            cameraPos = (cameraFront.clone() as Vector3f).cross(cameraUp).normalize().mulAdd(cameraSpeed, cameraPos)
        }
        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_SPACE)) {
            cameraPos = (cameraUp.clone() as Vector3f).mulAdd(cameraSpeed, cameraPos)
        }
        if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_LEFT_CONTROL)) {
            cameraPos = (cameraUp.clone() as Vector3f).mulAdd(-cameraSpeed, cameraPos)
        }
        if(MouseListener.dx != 0.0 || MouseListener.dy != 0.0){
            if(cameraInitialized){
                yaw -= MouseListener.dx*mouseSensitivity
                pitch = (-89.999).coerceAtLeast((89.999).coerceAtMost(pitch + MouseListener.dy * mouseSensitivity))

                val direction = Vector3f(
                    (Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch))).toFloat(),
                    Math.sin(Math.toRadians(pitch)).toFloat(),
                    (Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch))).toFloat()
                )
                cameraFront = direction.normalize()
            } else cameraInitialized = true
        }
    }

    val viewBuffer = BufferUtils.createFloatBuffer(16)
    val projectionBuffer = BufferUtils.createFloatBuffer(16)
    private fun updateUniformBuffer() {
        val ubo = UniformBuffer.viewAndProjectionMatrices
        ubo.updateSubData(

            0 to getProjectionMatrix().get(projectionBuffer.rewind()),
            1 to getViewMatrix().get(viewBuffer.rewind()),
        )
    }

}