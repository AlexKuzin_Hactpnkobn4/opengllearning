import org.lwjgl.glfw.GLFW.GLFW_PRESS
import org.lwjgl.glfw.GLFW.GLFW_RELEASE

object MouseListener {
    private val mouseButtonPressed = BooleanArray(3)
    var lastX = 0.0; private set
    var lastY = 0.0; private set

    var scrollX = 0.0; private set
    var scrollY = 0.0; private set

    var yPos = 0.0; private set
    var xPos = 0.0; private set

    var dx = 0.0;  private set
        get() = lastX - xPos
    var dy = 0.0; private set
        get() = lastY - yPos

    var isDragging = false; private set

    fun mousePosCallback(window: Long, xpos: Double, ypos: Double) {
        xPos = xpos
        yPos = ypos
        isDragging = mouseButtonPressed.any { isPressed -> isPressed }
    }

    fun mouseButtonCallback(window: Long, button: Int, action: Int, mods: Int) {
        if (action == GLFW_PRESS) {
            if (button < mouseButtonPressed.size) {
                mouseButtonPressed[button] = true
            }
        } else if (action == GLFW_RELEASE) {
            if (button < mouseButtonPressed.size) {
                mouseButtonPressed[button] = false
                isDragging = false
            }
        }
    }

    fun mouseScrollCallback(window: Long, xOffset: Double, yOffset: Double) {
        scrollX = xOffset
        scrollY = yOffset
    }

    fun isMouseButtonDown(buttonCode: Int): Boolean {
        return if (buttonCode < mouseButtonPressed.size) {
            mouseButtonPressed[buttonCode]
        } else false
    }

    fun clear(){
        lastX = xPos
        lastY = yPos
    }
}