package vao

import org.lwjgl.opengl.GL30.*
import toBuffer
import verticesTexturedPlate

class TexturedPlate {

    val vaoId: Int

    init {
        vaoId = glGenVertexArrays()
        val vboId = glGenBuffers()
        val eboId = glGenBuffers()
        glBindVertexArray(vaoId)
        glBindBuffer(GL_ARRAY_BUFFER, vboId)

        glBufferData(GL_ARRAY_BUFFER, verticesTexturedPlate.toBuffer(), GL_STATIC_DRAW)
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 5 * Float.SIZE_BYTES, 0L)
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 5 * Float.SIZE_BYTES, (3 * Float.SIZE_BYTES).toLong())
        glEnableVertexAttribArray(1)

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)

        glBindVertexArray(0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    }
}