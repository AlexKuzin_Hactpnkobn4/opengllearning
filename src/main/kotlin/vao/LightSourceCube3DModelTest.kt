package vao

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL15.glBufferData
import org.lwjgl.opengl.GL30.*
import toBuffer
import vertices3DCube
import vertices3DCubeWithNormalsAndTexCoords
import java.nio.FloatBuffer

class LightSourceCube3DModelTest {

    val vaoId: Int

    init {
        vaoId = glGenVertexArrays()
        val vboId = glGenBuffers()
        val eboId = glGenBuffers()
        glBindVertexArray(vaoId)
        glBindBuffer(GL_ARRAY_BUFFER, vboId)

        glBufferData(GL_ARRAY_BUFFER, vertices3DCube.toBuffer(), GL_STATIC_DRAW)
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * Float.SIZE_BYTES, 0L)
        glEnableVertexAttribArray(0)

        val textureVboId = glGenBuffers()
        glBindBuffer(GL_ARRAY_BUFFER, textureVboId)
//        glBufferData(GL_ARRAY_BUFFER, vertices3DCubeWithNormalsAndTexCoords.toBuffer(), GL_STATIC_DRAW)
//        glVertexAttribPointer(1, 2, GL_FLOAT, false, 8 * Float.SIZE_BYTES, (6 * Float.SIZE_BYTES).toLong())
//        glEnableVertexAttribArray(1)
        val data = BufferUtils.createFloatBuffer((vertices3DCube.size/3) * 2)
        for(i in 0 until data.capacity()/6){
            data.put(0f)
            data.put(0f)

            data.put(0f)
            data.put(1f)

            data.put(1f)
            data.put(1f)
        }
        glBufferData(GL_ARRAY_BUFFER, data.flip(), GL_STATIC_DRAW)
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 2 * Float.SIZE_BYTES, 0L)
        glEnableVertexAttribArray(1)

        val buffer = BufferUtils.createIntBuffer(vertices3DCube.size/3)
        (0 until buffer.capacity()).forEach{buffer.put(it)}
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer.flip() , GL_STATIC_DRAW)

        glBindVertexArray(0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    }
}