package vao

import org.lwjgl.opengl.GL15.glBufferData
import org.lwjgl.opengl.GL30.*
import screenQuad
import toBuffer

class ScreenQuadVao{

    val vaoId: Int

    init {
        vaoId = glGenVertexArrays()
        val vboId = glGenBuffers()
        glBindVertexArray(vaoId)
        glBindBuffer(GL_ARRAY_BUFFER, vboId)


        glBufferData(GL_ARRAY_BUFFER, screenQuad.toBuffer(), GL_STATIC_DRAW)
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * Float.SIZE_BYTES, 0L)
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())



        glBindVertexArray(0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    }
}