package vao

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL15.glBufferData
import org.lwjgl.opengl.GL30
import toBuffer
import vertices3DCube

class LightSourceCube {

    val vaoId: Int

    init {
        vaoId = GL30.glGenVertexArrays()
        val vboId = GL30.glGenBuffers()
        val eboId = GL30.glGenBuffers()
        GL30.glBindVertexArray(vaoId)
        GL30.glBindBuffer(GL30.GL_ARRAY_BUFFER, vboId)

        glBufferData(GL30.GL_ARRAY_BUFFER, vertices3DCube.toBuffer(), GL30.GL_STATIC_DRAW)
        GL30.glVertexAttribPointer(0, 3, GL30.GL_FLOAT, false, 3 * Float.SIZE_BYTES, 0L)
        GL30.glEnableVertexAttribArray(0)

        val buffer = BufferUtils.createIntBuffer(vertices3DCube.size/3)
        (0 until buffer.capacity()).forEach{buffer.put(it)}
        GL30.glBindBuffer(GL30.GL_ELEMENT_ARRAY_BUFFER, eboId)
        GL30.glBufferData(GL30.GL_ELEMENT_ARRAY_BUFFER, buffer.flip() , GL30.GL_STATIC_DRAW)

        GL30.glBindVertexArray(0)
        GL30.glBindBuffer(GL30.GL_ELEMENT_ARRAY_BUFFER, 0)
        GL30.glBindBuffer(GL30.GL_ARRAY_BUFFER, 0)
    }
}