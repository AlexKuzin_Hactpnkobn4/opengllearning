package vao

object VaoCache {
    private val cache = HashMap<Vao, Int>()

    fun getId(vao: Vao): Int{
        return cache.computeIfAbsent(vao){vao.creationFunc.invoke()}
    }

    enum class Vao(val creationFunc: () -> Int) {
        textured3DCube({Textured3DCube().vaoId}),
        texturedPlate({TexturedPlate().vaoId})
    }
}