package assimp

import org.lwjgl.opengl.GL30.*
import java.nio.FloatBuffer
import java.nio.IntBuffer

class Mesh(
    positions: FloatBuffer,
    texCoords: FloatBuffer,
    private val indices: IntBuffer,
) {
    val vaoId = glGenVertexArrays()
    private val vboIds = ArrayList<Int>()
    val numVertices:Int get() = indices.capacity()

    init {
        glBindVertexArray(vaoId)

        // Positions VBO
        var vboId = genVboId()
        glBindBuffer(GL_ARRAY_BUFFER, vboId)
        glBufferData(GL_ARRAY_BUFFER, positions.flip(), GL_STATIC_DRAW)
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0)

        // Texture coordinates VBO
        vboId = genVboId()
        glBindBuffer(GL_ARRAY_BUFFER, vboId)
        glBufferData(GL_ARRAY_BUFFER, texCoords.flip(), GL_STATIC_DRAW)
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0)

        // Index VBO
        vboId = genVboId()
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.flip(), GL_STATIC_DRAW)

        glBindVertexArray(0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    }

    fun genVboId(): Int{
        val vboId = glGenBuffers()
        vboIds.add(vboId)
        return vboId
    }
}