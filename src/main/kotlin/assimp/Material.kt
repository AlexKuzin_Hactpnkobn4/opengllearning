package assimp

import org.joml.Vector4f

class Material {

    companion object{
        val defaultColor = Vector4f(0.0f, 0.0f, 0.0f, 1.0f)
        val defaultMaterial = Material()
    }

    var defuseColor = defaultColor
    var texPath: String? = null

    val meshList = ArrayList<Mesh>()



}