package assimp

import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.assimp.AIColor4D
import org.lwjgl.assimp.AIMaterial
import org.lwjgl.assimp.AIMesh
import org.lwjgl.assimp.AIString
import org.lwjgl.assimp.Assimp.*
import org.lwjgl.system.MemoryStack
import java.io.File
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.util.*

const val flags = aiProcess_GenSmoothNormals        or
                  aiProcess_JoinIdenticalVertices   or
                  aiProcess_Triangulate             or
                  aiProcess_FixInfacingNormals      or
                  aiProcess_CalcTangentSpace        or
                  aiProcess_LimitBoneWeights        or
                  aiProcess_PreTransformVertices

fun loadModel(path: String): Model {
    val file = File(path)
    if (!file.exists()) {
        throw RuntimeException("Model path does not exist [$path]")
    }
    val modelDir = file.parent
    val scene = aiImportFile(path, flags) ?: throw RuntimeException("Error loading model [modelPath: $path]")

    val materials = mutableListOf<Material>()
    for (i in 0 until scene.mNumMaterials()) {
        val aiMaterial = AIMaterial.create(scene.mMaterials()!!.get(i))
        materials.add(processMaterials(aiMaterial, modelDir))
    }

    val defaultMaterial = Material()
    for (i in 0 until scene.mNumMeshes()) {
        val aiMesh = AIMesh.create(scene.mMeshes()!!.get(i))
        val mesh = processMesh(aiMesh)

        val materialIndex = aiMesh.mMaterialIndex()
        val material =
            if (materialIndex in 0..materials.size) {
                materials[materialIndex]
            } else defaultMaterial

        material.meshList.add(mesh)
    }

    if(defaultMaterial.meshList.isNotEmpty()){
        materials.add(defaultMaterial)
    }

    return Model(materials)
}

fun processMaterials(aiMaterial: AIMaterial, modelDir: String): Material {
    val material = Material()
    MemoryStack.stackPush().use { stack ->
        val colorBuffer = AIColor4D.create()
        val result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, colorBuffer)

        if (result == aiReturn_SUCCESS) {
            material.defuseColor = with(colorBuffer) { Vector4f(r(), g(), b(), a()) }
        }

        val aiTexPath = AIString.calloc(stack)
        aiGetMaterialTexture(
            aiMaterial, aiTextureType_DIFFUSE, 0, aiTexPath,
            null as IntBuffer?, null, null, null, null, null
        )

        val texPath = aiTexPath.dataString()
        if (texPath.isNotEmpty()) {
            material.texPath = modelDir + File.separator + File(texPath).name
            TextureCache.createTexture(material.texPath!!)
            material.defuseColor = Material.defaultColor
        }
    }
    return material
}

fun processMesh(aiMesh: AIMesh): Mesh {
    val positionsBuffer = BufferUtils.createFloatBuffer(aiMesh.mVertices().remaining() * 3)
    val aiVertices = aiMesh.mVertices()
    while (aiVertices.hasRemaining()) {
        val vertCoords = aiVertices.get()
        positionsBuffer.put(vertCoords.x())
        positionsBuffer.put(vertCoords.y())
        positionsBuffer.put(vertCoords.z())
    }
    aiVertices.clear().close()

    val aiTexCoords = aiMesh.mTextureCoords(0)
    val texCoordsBuffer: FloatBuffer
    if (aiTexCoords == null) {
        texCoordsBuffer = BufferUtils.createFloatBuffer(0)
    } else {
        texCoordsBuffer = BufferUtils.createFloatBuffer(aiTexCoords.remaining() * 2)
        while (aiTexCoords.hasRemaining()) {
            val texCoord = aiTexCoords.get()
            texCoordsBuffer.put(texCoord.x())
            texCoordsBuffer.put(1 - texCoord.y())
        }
        aiTexCoords.clear().close()
    }

    val indicesList = LinkedList<Int>()
    val aiFaces = aiMesh.mFaces()
    while (aiFaces.hasRemaining()) {
        val face = aiFaces.get()
        val aiIndices = face.mIndices()
        while (aiIndices.hasRemaining()) {
            indicesList.add(aiIndices.get())
        }
    }
    val indicesBuffer = BufferUtils.createIntBuffer(indicesList.size)
    indicesList.forEach { indicesBuffer.put(it) }

    return Mesh(positionsBuffer, texCoordsBuffer, indicesBuffer)
}
