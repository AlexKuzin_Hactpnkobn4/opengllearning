package assimp

import ShaderFile
import org.joml.Matrix4f
import org.lwjgl.BufferUtils
import org.lwjgl.assimp.AITexture
import org.lwjgl.opengl.GL11.glBindTexture
import org.lwjgl.opengl.GL11.glTexParameteri
import org.lwjgl.opengl.GL30.*
import org.lwjgl.stb.STBImage
import org.lwjgl.stb.STBImage.stbi_load
import toBuffer
import java.nio.ByteBuffer

class Texture {
    val id : Int = glGenTextures()
    val filename: String

    private val widthBuffer = BufferUtils.createIntBuffer(1)
    private val heightBuffer = BufferUtils.createIntBuffer(1)
    private val channelsBuffer = BufferUtils.createIntBuffer(1)
    private val width: Int  get() = widthBuffer.get()
    private val height: Int get() = heightBuffer.get()
    private val channels: Int get() = channelsBuffer.get()

    constructor(texture: AITexture){
        val image = STBImage.stbi_load_from_memory(texture.pcDataCompressed(), widthBuffer, heightBuffer, channelsBuffer, 0)
        filename = texture.mFilename().dataString()
        create2DTexture(image)
    }

    constructor(path: String){
        val image = stbi_load(path, widthBuffer, heightBuffer, channelsBuffer, 4)
        filename = path.split("/").last()
        create2DTexture(image)
    }

    private fun create2DTexture(image: ByteBuffer?) {
        if(image == null) throw RuntimeException("Error: (Texture) could not load image '${filename}'")

        val format = when(channels){
            1 -> GL_COLOR_INDEX
            2 -> GL_ALPHA
            3 -> GL_RGBA
            4 -> GL_RGBA
            else -> throw RuntimeException("Error: (Texture) unknown number of channels '${channels}'")
        }
        glBindTexture(GL_TEXTURE_2D, id)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            format,
            width,
            height,
            0,
            format,
            GL_UNSIGNED_BYTE,
            image
        )

        STBImage.stbi_image_free(image)
    }

}