package assimp

object TextureCache {

    val defaultTexturePath = "res/textures/"
    val defaultTextureFilname = "default_texture.png"

    val textureMap = HashMap<String, Texture>()

    fun createTexture(path: String): Texture = textureMap.computeIfAbsent(path) { Texture(path) }

    fun getTexture(path: String?): Texture {
        return if (path == null) {
            createTexture(defaultTexturePath + defaultTextureFilname)
        } else createTexture(path)
    }

    fun getByFilename(fileName: String?): Texture{
        return getTexture(defaultTexturePath + fileName)
    }

}