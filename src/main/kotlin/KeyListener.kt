import org.lwjgl.glfw.GLFW.GLFW_PRESS
import org.lwjgl.glfw.GLFW.GLFW_RELEASE

object KeyListener {
    private val keyPressed = BooleanArray(350)
    private val keyJustPressed = BooleanArray(350)
    private val keyJustReleased = BooleanArray(350)

    fun keyCallback(window: Long, key: Int, scancode: Int, action: Int, mode: Int) {
        if (action == GLFW_PRESS) {
            keyPressed[key] = true
            keyJustPressed[key] = true

            keyJustReleased[key] = false
        } else if (action == GLFW_RELEASE) {
            keyPressed[key] = false
            keyJustPressed[key] = false

            keyJustReleased[key] = true
        }
    }

    fun isKeyPressed(keyCode: Int) = keyPressed[keyCode]
    fun isKeyJustPressed(keyCode: Int) = keyJustPressed[keyCode]
    fun isKeyJustReleased(keyCode: Int) = keyJustReleased[keyCode]

    fun clear(){
        keyJustPressed.fill(false)
        keyJustReleased.fill(false)
    }
}