import imgui.ImGui
import imgui.flag.ImGuiConfigFlags
import imgui.gl3.ImGuiImplGl3
import imgui.glfw.ImGuiImplGlfw

object GuiLayer {
    val imGuiGlfw = ImGuiImplGlfw()
    val imGuiGl3 = ImGuiImplGl3()

    private var guiCallback: () -> Unit = {}

    fun init(){
        ImGui.createContext()

        configure()

        imGuiGlfw.init(Window.window, true)
        imGuiGl3.init("#version 330 core")
    }

    private fun configure() {
        ImGui.styleColorsDark()

        val io = ImGui.getIO()
        io.addConfigFlags(ImGuiConfigFlags.ViewportsEnable)
        io.iniFilename = null
        io.configViewportsNoTaskBarIcon = true
    }

    fun setGuiCallback(callback: () -> Unit){
        guiCallback = callback
    }

    fun draw(){
        imGuiGlfw.newFrame()
        ImGui.newFrame()

        if(ImGui.beginMainMenuBar()){
            ImGui.text("press 'Esc' to open render menu")
            ImGui.endMainMenuBar()
        }

        guiCallback.invoke()

        ImGui.render()
        imGuiGl3.renderDrawData(ImGui.getDrawData())
    }

    fun dispose(){
        imGuiGl3.dispose()
        imGuiGlfw.dispose()
        ImGui.destroyContext()
    }
}