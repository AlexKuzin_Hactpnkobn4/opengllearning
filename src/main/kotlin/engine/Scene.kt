package engine

import Camera
import FpsCamera
import org.joml.Math
import org.joml.Matrix4f
import org.joml.Vector3f

class Scene {
    val camera: Camera = FpsCamera().also { it.cameraPos = Vector3f(0f, 1.3f, 8f) }
    val projection = Matrix4f().perspective(Math.toRadians(45f), 800f / 600f, 0.1f, 100f)

}