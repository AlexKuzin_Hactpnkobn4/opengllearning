package engine

import Window
import Window.window
import assimp.TextureCache
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL11.glEnable
import org.lwjgl.opengl.GL30
import java.io.File
import java.nio.file.Paths

class Game(
    val texturesPreload: Boolean = false,
    val shadersPreload: Boolean = false
) {
    private var scene: Scene = Scene()
    private var cursorIsVisible = false


    fun start(init: (scene: Scene) -> () -> Unit) {
        var x: () -> Unit = {}
        Window.run(
            init = {
                if(shadersPreload) {resolvePreload()}

                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)
                glEnable(GL30.GL_DEPTH_TEST)
                x = init.invoke(scene)
            },
            loop = {
                x.invoke()
                handleCursor()
                if(!cursorIsVisible){
                    scene.camera.update()
                }
            }
        )
    }

    private fun handleCursor() {
        if(KeyListener.isKeyJustPressed(GLFW_KEY_ESCAPE)){
            if(cursorIsVisible){
                cursorIsVisible = false
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)
            } else if (!cursorIsVisible) {
                cursorIsVisible = true
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL)
            }
        }
    }

    private fun resolvePreload() {
        if (texturesPreload) {
            val textureFolder = File(Paths.get(TextureCache.defaultTexturePath).toUri())
            textureFolder.listFiles()?.forEach {
                if (it.isFile) {
                    val split = it.name.split(".")
                    if (split.size <= 1 || !supportedExtensions.contains(split.last())) {
                        System.err.println("File ${it.absolutePath} cannot be preloaded as a texture: unsupported extension.")
                    } else {
                        with(TextureCache){ createTexture(defaultTexturePath + it.name)}
                    }
                }
            }
            println(textureFolder.isDirectory)
        }
    }

    val supportedExtensions = arrayListOf("png", "jpg", "jpeg")


    fun loop() {

    }


}

fun loop(loop: () -> Unit): () -> Unit = loop