import org.lwjgl.opengl.GL20.*
import java.util.regex.Pattern


class ShaderFile(
    filePath: String
) {
    val shaderProgramId: Int
    private var beingUsed = false
    private val shaders = mutableListOf<Shader>()

    init {
        val source = loadFromFile(filePath)

        val pattern = Pattern.compile("(#type) (.*)(\\s+)")
        val matcher = pattern.matcher(source)

        if (matcher.find()) {
            var lastMatchStartIndex = matcher.end()
            var lastMatchShaderType: Shader.Type = Shader.Type.fromString(matcher.group(2))

            while (matcher.find()) {
                val shaderString = source.substring(lastMatchStartIndex, matcher.start())
                shaders.add(Shader(shaderString, lastMatchShaderType))

                lastMatchStartIndex = matcher.end()
                lastMatchShaderType = Shader.Type.fromString(matcher.group(2))
            }
            val shaderString = source.substring(lastMatchStartIndex, source.lastIndex + 1)
            shaders.add(Shader(shaderString, lastMatchShaderType))
        } else {
            throw RuntimeException("Incorrect shader file - can't find any type headers in file '$filePath'")
        }

        shaderProgramId = glCreateProgram()
        shaders.forEach { shader -> glAttachShader(shaderProgramId, shader.id) }
        glLinkProgram(shaderProgramId)

        val success = glGetProgrami(shaderProgramId, GL_LINK_STATUS)
        if (success == GL_FALSE) {
            val len = glGetProgrami(shaderProgramId, GL_INFO_LOG_LENGTH)
            val errorMessage = glGetProgramInfoLog(shaderProgramId, len)
            println("ERROR: '$filePath' \n\tLinking of shaders failed: $errorMessage")
            throw RuntimeException(errorMessage)
        }
    }

    class Shader(
        src: String,
        type: Type
    ) {
        val id: Int

        init {
            id = glCreateShader(type.glTypeId)
            // Compilation
            glShaderSource(id, src)
            glCompileShader(id)

            val success = glGetShaderi(id, GL_COMPILE_STATUS)
            if (success == GL_FALSE) {
                val len = glGetShaderi(id, GL_INFO_LOG_LENGTH)
                val errorMessage = glGetShaderInfoLog(id, len)
                throw RuntimeException(("${type.name} shader with compilation failed! \n\rsrc: '$src' \n\r ERROR: $errorMessage"))
            }
        }

        enum class Type(val glTypeId: Int) {
            vertex(GL_VERTEX_SHADER),
            fragment(GL_FRAGMENT_SHADER);

            companion object {
                fun fromString(typeString: String) =
                    if (values().any { type -> type.name == typeString.lowercase() }) {
                        valueOf(typeString)
                    } else throw RuntimeException("Incorrect shader type string: $typeString")
            }
        }
    }
}
