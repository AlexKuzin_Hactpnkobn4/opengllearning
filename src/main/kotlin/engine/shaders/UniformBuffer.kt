package engine.shaders

import org.joml.*
import org.lwjgl.opengl.ARBUniformBufferObject.GL_UNIFORM_BUFFER
import org.lwjgl.opengl.ARBUniformBufferObject.glBindBufferBase
import org.lwjgl.opengl.GL30.*
import java.nio.FloatBuffer
import kotlin.reflect.KClass

enum class UniformBuffer(vararg classes: KClass<*>) {
    viewAndProjectionMatrices(Matrix4f::class, Matrix4f::class),
    ;

    companion object{
        val uboIds = mutableMapOf<UniformBuffer, Int>()
        val bindingPoint = mutableMapOf<UniformBuffer, Int>()
    }
    val uniformCount = classes.size
    val bufferSize: Long
    val uniformOffsets = mutableMapOf<Int, Long>(0 to 0)

    init {
        var bufferSize = 0L
        classes.forEachIndexed { index, kClass ->
            if(typeSizeInBytes.containsKey(kClass)){
                val uniformSize = typeSizeInBytes[kClass]!!
                uniformOffsets[index+1] = uniformOffsets.getOrDefault(index,0) + uniformSize
                bufferSize += uniformSize
            } else throw RuntimeException("There is no information about size in bytes of class '${kClass.qualifiedName}'")
        }
        this.bufferSize = bufferSize
    }

    fun getUboId() : Int {
        return uboIds.computeIfAbsent(this){
            val uboId = glGenBuffers()
            glBindBuffer(GL_UNIFORM_BUFFER, uboId)
            glBufferData(GL_UNIFORM_BUFFER, bufferSize, GL_DYNAMIC_DRAW)
            uboId
        }
    }

    fun getBindingPoint(): Int{
        return bindingPoint.computeIfAbsent(this){
            glBindBufferBase(GL_UNIFORM_BUFFER, this.ordinal, getUboId())
            ordinal
        }
    }

    fun getUniformOffset(index: Int): Long{
        return uniformOffsets.getOrElse(index){throw RuntimeException("Index '$index' is out of bounds for uniformBuffer '${this.name}'. Max index: ${uniformOffsets.keys.max()}")}
    }

    fun updateSubData(vararg data: Pair<Int, FloatBuffer>){

        glBindBuffer(GL_UNIFORM_BUFFER, getUboId())
        data.forEach { (index, data) ->
            if(index >= uniformCount){
                throw RuntimeException("index $index is out of bounds for buffer '${this.name}'. Max count = $uniformCount")
            }
            glBufferSubData(GL_UNIFORM_BUFFER, getUniformOffset(index), data)
        }
        glBindBuffer(GL_UNIFORM_BUFFER, 0)

    }

}

private val typeSizeInBytes = mapOf(
    Matrix4f::class to 16*Float.SIZE_BYTES,
    Matrix3f::class to 9*Float.SIZE_BYTES,
    Vector4f::class to 4*Float.SIZE_BYTES,
    Vector3f::class to 3*Float.SIZE_BYTES,
    Vector2f::class to 2*Float.SIZE_BYTES,
    Float::class    to Float.SIZE_BYTES,
    Float::class    to Float.SIZE_BYTES,
)