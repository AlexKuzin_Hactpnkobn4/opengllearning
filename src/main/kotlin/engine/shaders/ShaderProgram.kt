package engine.shaders

import engine.GameEntity
import engine.Scene
import org.joml.Matrix4f
import org.joml.Vector4f
import org.lwjgl.opengl.ARBUniformBufferObject.glGetUniformBlockIndex
import org.lwjgl.opengl.ARBUniformBufferObject.glUniformBlockBinding
import org.lwjgl.opengl.GL30.*

class ShaderProgram(val id: Int) {
    private val uniformLocations = mutableMapOf<String, Int>()

    private fun getLocation(name: String) = uniformLocations.getOrPut(name){glGetUniformLocation(id, name)}

    fun setUniform(name: String, value: Int){
        glUniform1i(getLocation(name), value)
    }

    fun setUniform(name: String, value: Matrix4f){
        glUniformMatrix4fv(getLocation(name), false, value.get(FloatArray(16)))
    }

    fun setUniform(name: String, value: Vector4f){
        glUniform4f(getLocation(name), value.x, value.y, value.z, value.w)
    }

    fun setUniformBuffer(name: String, uniformBuffer: UniformBuffer){
        val uniformBlockId = glGetUniformBlockIndex(id, name)
        glUniformBlockBinding(id, uniformBlockId, uniformBuffer.getBindingPoint())
    }

    fun setTextureUniform(name: String, texId: Int, texNum: Int = 0){
        val textureSlot = GL_TEXTURE0 + texNum
        glActiveTexture(textureSlot)
        glBindTexture(GL_TEXTURE_2D, texId)
        setUniform(name, texNum)
    }

    fun setSceneUniforms(scene: Scene) {
        setUniform("projection", scene.projection)
        setUniform("view", scene.camera.getViewMatrix())
    }

    fun use(block: (ShaderProgram) -> Unit) {
        glUseProgram(id)
        block.invoke(this)
        glUseProgram(0)
    }

    fun use(scene: Scene, block: (ShaderProgram) -> Unit) {
        glUseProgram(id)
        setSceneUniforms(scene)
        block.invoke(this)
        glUseProgram(0)
    }

    fun drawEntity(entity: GameEntity) {
        glBindVertexArray(entity.vaoId)
        this.setUniform("model", entity.modelMatrix)
        entity.textures.forEachIndexed{ index, texId ->
            if(texId >= 0){

                glActiveTexture(GL_TEXTURE0 + index)
                glBindTexture(GL_TEXTURE_2D, texId)
                this.setTextureUniform("texture${index + 1}", texId)
            }
        }
        glDrawArrays(GL_TRIANGLES, 0, 36)
    }
}