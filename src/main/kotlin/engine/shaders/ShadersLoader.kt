package engine.shaders

import ShaderFile

class ShadersLoader {

    companion object {
        private const val defaultShaderFolder = "res/shaders/"
        var shaderFolder = defaultShaderFolder

        fun load(path: String): ShaderProgram {
            val shaderFile = ShaderFile(shaderFolder + path)

            val shaderProgram = ShaderProgram(shaderFile.shaderProgramId)
            shaderProgram.setUniformBuffer("projectionAndView", UniformBuffer.viewAndProjectionMatrices)
            return shaderProgram
        }
    }
}