package engine

import assimp.TextureCache
import org.joml.Matrix4f
import org.joml.Quaternionf
import org.joml.Vector3f
import vao.VaoCache

class GameEntity(val vaoId: Int, vararg textureIds: Int) {
    companion object{
        const val MAX_TEXTURES_COUNT = 8
    }

    val modelMatrix = Matrix4f().identity()
    val position = Vector3f(0f, 0f, 0f)
    val rotation = Quaternionf()

    var scale: Number = 1.0f

    val textures = IntArray(MAX_TEXTURES_COUNT).also {
        it.fill(-1)
        textureIds.forEachIndexed{index, value -> it[index] = value }
    }

    constructor(vaoId: Int, posX: Float, posY: Float, posZ: Float) : this(vaoId) {
        with(position) {
            x = posX
            y = posY
            z = posZ
        }
    }

    constructor(cachedVao: VaoCache.Vao, vararg textureFilenames: String ) : this(VaoCache.getId(cachedVao), *textureFilenames.map {TextureCache.getByFilename(it).id}.toIntArray())

    fun setPosition(posX: Number, posY: Number, posZ: Number) {
        with(this.position) {
            x = posX.toFloat()
            y = posY.toFloat()
            z = posZ.toFloat()
        }
        println(this.position)
    }

    fun setRotation(x: Number, y: Number, z: Number, angle: Number) {
        this.rotation.fromAxisAngleRad(x.toFloat(), y.toFloat(), z.toFloat(), Math.toRadians(angle.toDouble()).toFloat())
        updateModelMatrix()
    }

    fun rotateAxisX(angle: Number): GameEntity{
        setRotation(1,0,0, angle)
        return this
    }

    fun rotateAxisY(angle: Number): GameEntity {
        setRotation(0,1,0, angle)
        return this
    }

    fun rotateAxisZ(angle: Number): GameEntity {
        setRotation(0,0,1, angle)
        return this
    }

    fun setTexture(index: Int, texId: Int){
        if(index >= MAX_TEXTURES_COUNT ){
            throw RuntimeException("Can't attach texture for index $index - max index is $MAX_TEXTURES_COUNT")
        } else{
            textures[index-1] = texId
        }
    }

    fun updateModelMatrix() {
        modelMatrix.translationRotateScale(position, rotation, scale.toFloat())
    }
}