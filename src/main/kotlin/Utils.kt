import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL20
import java.io.IOException
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.nio.file.Files
import java.nio.file.Paths

fun loadFromFile(filePath: String): String {
    try {
        return String(Files.readAllBytes(Paths.get(filePath)))
    } catch (e: IOException) {
        throw RuntimeException(e)
    }
}

fun FloatArray.toBuffer(): FloatBuffer {
    val buffer = BufferUtils.createFloatBuffer(this.size)
    buffer.put(this).flip()
    return buffer
}

fun IntArray.toBuffer(): IntBuffer {
    val buffer = BufferUtils.createIntBuffer(this.size)
    buffer.put(this).flip()
    return buffer
}