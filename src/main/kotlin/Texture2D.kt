import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL30.*
import org.lwjgl.stb.STBImage.*

class Texture2D(filePath: String) {
    val texID: Int = glGenTextures()

    init {
        glBindTexture(GL_TEXTURE_2D, texID)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

        val width = BufferUtils.createIntBuffer(1)
        val height = BufferUtils.createIntBuffer(1)
        val channels = BufferUtils.createIntBuffer(1)
        val image = stbi_load(filePath, width, height, channels, 0)

        if (image != null) {
            val format = when(channels.get(0)){
                3 -> GL_RGB
                4 -> GL_RGBA
                else -> throw RuntimeException("Error: (Texture) unknown number of channels '${channels.get(0)}'")
            }
            glTexImage2D(GL_TEXTURE_2D, 0, format, width.get(0), height.get(0), 0, format, GL_UNSIGNED_BYTE, image)
        } else{
            throw RuntimeException("Error: (Texture) could not load image '$filePath'")
        }

        stbi_image_free(image)
    }
}