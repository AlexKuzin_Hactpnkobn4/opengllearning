import vao.Box3D
import vao.LightSourceCube
import Window.window
import org.joml.*
import org.joml.Math.*
import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import kotlin.random.Random

class `10_Light_source_types` {

    @Test
    fun directional(){
        var box3DShaderProgramId = -1

        var cubes3DVaoId = -1

        var boxTextureId = -1
        var boxReflectionTextureId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*7,(Random.nextFloat()-0.5f)*4+6,(Random.nextFloat()-0.5f)*7-5))}
            .map{ it to 0.02f + Random.nextFloat()/12}
            .toMutableList()

        val lightDir = Vector3f(-2f, 10f, -2f)
        
        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)

                cubes3DVaoId = Box3D().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/10_Directional_light.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                boxTextureId = Texture2D("res/textures/boxSide.png").texID
                glActiveTexture(GL_TEXTURE1)
                boxReflectionTextureId = Texture2D("res/textures/boxSideReflectionMap.png").texID
            },
            loop = {
                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.direction"), lightDir.x, lightDir.y, lightDir.z)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.ambient"), 0.2f, 0.2f, 0.2f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.diffuse"), 0.9f, 0.9f, 0.9f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.specular"), 0.6f, 0.6f, 0.6f)

                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 0)
                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 1)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),32f)
                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, boxTextureId)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, boxReflectionTextureId)

                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, pair.first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }

    @Test
    fun pointLight(){
        var box3DShaderProgramId = -1
        var lightingSourceShaderProgramId = -1

        var cubes3DVaoId = -1
        var lightSource3DVaoId = -1

        var boxTextureId = -1
        var boxReflectionTextureId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*9,(Random.nextFloat()-0.5f)*6+6,(Random.nextFloat()-0.5f)*9-5))}
            .map{ it to 0.02f + Random.nextFloat()/22}
            .toMutableList()

        val lightPos = Vector3f(0f, 2.5f, -7f)


        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)
                glClearColor(0.1f, 0.1f, 0.1f, 1f)

                cubes3DVaoId = Box3D().vaoId
                lightSource3DVaoId = LightSourceCube().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/10_Point_light.glsl").shaderProgramId
                lightingSourceShaderProgramId = ShaderFile("res/shaders/07_Colors_and_lighting_source.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                boxTextureId = Texture2D("res/textures/boxSide.png").texID
                glActiveTexture(GL_TEXTURE1)
                boxReflectionTextureId = Texture2D("res/textures/boxSideReflectionMap.png").texID
            },
            loop = {
                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.position"), lightPos.x, lightPos.y, lightPos.z)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.ambient"), 0.2f, 0.2f, 0.2f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.diffuse"), 0.7f, 0.7f, 0.7f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.specular"), 0.9f, 0.9f, 0.9f)

                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.constant"),1f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.linear"),0.03f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.quadratic"),0.03f)

                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 0)
                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 1)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),32f)
                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, boxTextureId)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, boxReflectionTextureId)

                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, pair.first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                glUseProgram(lightingSourceShaderProgramId)
                glBindVertexArray(lightSource3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                GL20.glUniformMatrix4fv(
                    glGetUniformLocation(lightingSourceShaderProgramId, "model"),
                    false,
                    Matrix4f().identity().translate(lightPos).scale(Vector3f(0.2f)).get(FloatArray(16))
                )

                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }

    @Test
    fun projectorLight(){
        var box3DShaderProgramId = -1

        var cubes3DVaoId = -1

        var boxTextureId = -1
        var boxReflectionTextureId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*9,(Random.nextFloat()-0.5f)*6+6,(Random.nextFloat()-0.5f)*9-5))}
            .map{ it to 0.02f + Random.nextFloat()/22}
            .toMutableList()



        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)
                glClearColor(0.1f, 0.1f, 0.1f, 1f)

                cubes3DVaoId = Box3D().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/10_Flashlight.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                boxTextureId = Texture2D("res/textures/boxSide.png").texID
                glActiveTexture(GL_TEXTURE1)
                boxReflectionTextureId = Texture2D("res/textures/boxSideReflectionMap.png").texID
            },
            loop = {
                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.position"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.direction"), camera.cameraFront.x, camera.cameraFront.y, camera.cameraFront.z)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.cutoff"), cos(toRadians(12.5f)))
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.outerCutoff"), cos(toRadians(17.5f)))

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.ambient"), 0.2f, 0.2f, 0.2f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.diffuse"), 0.7f, 0.7f, 0.7f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.specular"), 0.9f, 0.9f, 0.9f)

                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.constant"),1f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.linear"),0.03f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "light.quadratic"),0.03f)

                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 0)
                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 1)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),32f)
                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, boxTextureId)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, boxReflectionTextureId)

                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, pair.first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }
}