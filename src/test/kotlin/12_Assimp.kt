import IOUtils.ioResourceToByteBuffer
import Window.HEIGHT
import Window.WIDTH
import Window.window
import assimp.Model
import assimp.TextureCache
import assimp.loadModel
import org.joml.Math.toRadians
import org.joml.Matrix4f
import org.joml.Vector3f
import org.junit.Test
import org.lwjgl.BufferUtils
import org.lwjgl.BufferUtils.createFloatBuffer
import org.lwjgl.BufferUtils.createIntBuffer
import org.lwjgl.assimp.*
import org.lwjgl.assimp.Assimp.aiProcess_EmbedTextures
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import org.lwjgl.stb.STBImage
import org.lwjgl.stb.STBImage.stbi_load_from_memory
import org.lwjgl.system.MemoryUtil
import vao.LightSourceCube
import vao.ToiletVaoObj
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.FloatBuffer
import kotlin.random.Random

class `12_Assimp` {


    @Test
    fun multipleLightSources() {
        WIDTH *= 2
        HEIGHT *= 2

        println(WIDTH)

        var shaderProgramId = -1
        var vaoId = -1

        val vertData = mutableListOf<FloatBuffer>()
        val textIds = mutableListOf<Int>()

        val vertices = floatArrayOf(
            //position        /texCoord
            -0.9f, -0.9f, 0f, 0f,//bottom-left
            0.9f, -0.9f,  2f, 0f,//bottom-right
            0.9f, 0.9f,   2f, 2f,//top-middle
            -0.9f, 0.9f,  0f, 2f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )


        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)

                AIFileIO.create()
                    .OpenProc { pFileIO: Long, fileName: Long, openMode: Long ->
                        val data: ByteBuffer
                        val fileNameUtf8 = MemoryUtil.memUTF8(fileName)
                        try {
                            data = ioResourceToByteBuffer(fileNameUtf8, 8192)
                        } catch (e: IOException) {
                            throw RuntimeException("Could not open file: $fileNameUtf8")
                        }
                        AIFile.create()
                            .ReadProc { pFile: Long, pBuffer: Long, size: Long, count: Long ->
                                val max = Math.min(data.remaining().toLong(), size * count)
                                MemoryUtil.memCopy(MemoryUtil.memAddress(data) + data.position(), pBuffer, max)
                                max
                            }
                            .SeekProc { pFile: Long, offset: Long, origin: Int ->
                                if (origin == Assimp.aiOrigin_CUR) {
                                    data.position(data.position() + offset.toInt())
                                } else if (origin == Assimp.aiOrigin_SET) {
                                    data.position(offset.toInt())
                                } else if (origin == Assimp.aiOrigin_END) {
                                    data.position(data.limit() + offset.toInt())
                                }
                                0
                            }
                            .FileSizeProc { pFile: Long -> data.limit().toLong() }
                            .address()
                    }
                    .CloseProc { pFileIO: Long, pFile: Long ->
                        val aiFile = AIFile.create(pFile)
                        aiFile.ReadProc().free()
                        aiFile.SeekProc().free()
                        aiFile.FileSizeProc().free()
                    }

                val scenePath = "res/3DModels/toilet/backpack.obj"
                val scene = Assimp.aiImportFile(
                    scenePath,
                    Assimp.aiProcess_Triangulate or aiProcess_EmbedTextures,

                    ) ?: throw RuntimeException("Scene located in path $scenePath is null")
                println("mNumTextures " + scene.mNumTextures())
                for(i in 0 until 3){
                    val texture = AITexture.create(scene.mTextures()!!.get(i))

                    textIds.add(processTexture(texture))
                }

                for (index in 0 until scene.mNumMeshes()) {
                    val mesh = AIMesh.create(
                        scene.mMeshes()?.get(index) ?: throw RuntimeException("Mesh with index $index is null")
                    )
                    val verticesBuffer = createFloatBuffer(mesh.mNumVertices() * 8)

                    for (vertIndex in 0 until mesh.mNumVertices()) {
                        val position = mesh.mVertices().get(vertIndex)
                        val normal = mesh.mNormals()?.get(vertIndex)
                        val texCord = mesh.mTextureCoords(0)?.get(vertIndex)

                        verticesBuffer.put(position.x())
                        verticesBuffer.put(position.y())
                        verticesBuffer.put(position.z())

                        verticesBuffer.put(normal?.x() ?: 0f)
                        verticesBuffer.put(normal?.y() ?: 0f)
                        verticesBuffer.put(normal?.z() ?: 0f)

                        verticesBuffer.put(texCord?.x() ?: 0f)
                        verticesBuffer.put(texCord?.y() ?: 0f)

                    }
                    vertData.add(verticesBuffer)

                    val indicesBuffer = createIntBuffer(mesh.mNumFaces() * mesh.mFaces().get(0).mNumIndices())

                    for (i in 0 until mesh.mNumFaces()) {
                        val face = mesh.mFaces().get(i)
                        for (faceIndex in 0 until face.mNumIndices()) {
                            indicesBuffer.put(face.mIndices().get(faceIndex))
                        }
                    }
                }

                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/12_Assimp.glsl").shaderProgramId


                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
            },
            loop = {

                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, textIds[2])

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                glBindVertexArray(0)
                glUseProgram(0)

//                camera.update()
            }
        )
    }

    @Test
    fun render3dModel() {
        WIDTH *= 2
        HEIGHT *= 2

        var shaderProgramId = -1

        var lightingSourceShaderProgramId = -1
        var lightSource3DVaoId = -1
        val pointLights = (1..4)
            .map { Vector3f((Random.nextFloat()-0.5f)*18,(Random.nextFloat()-0.5f)*6,(Random.nextFloat()-0.5f)*18-5) }

        val projection = Matrix4f().perspective(toRadians(45f), 800f / 600f, 0.1f, 100f)
        val camera = FpsCamera()



        var model: Model? = null

        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)

                shaderProgramId = ShaderFile("res/shaders/12_Render3dModel.glsl").shaderProgramId
                lightingSourceShaderProgramId = ShaderFile("res/shaders/07_Colors_and_lighting_source.glsl").shaderProgramId
                lightSource3DVaoId = LightSourceCube().vaoId
//                val scenePath = "res/3DModels/Toilet.3DS"
                val scenePath = "res/3DModels/toilet/backpack.obj"
//                val scenePath = "res/3DModels/Cup.obj"
//                val scenePath = "res/3DModels/cube/cube.obj"
                model = loadModel(scenePath)
            },
            loop = {
                glUseProgram(shaderProgramId)

                glUniformMatrix4fv(glGetUniformLocation(shaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(shaderProgramId, "projection"), false, projection.get(FloatArray(16)))

                glUniform1i(glGetUniformLocation(shaderProgramId, "texSampler"), 0)
                model!!.materialList.forEach{
                    glUniform4f(glGetUniformLocation(shaderProgramId, "material.diffuse"), it.defuseColor.x, it.defuseColor.y, it.defuseColor.z, it.defuseColor.w )
                    val texture = TextureCache.getTexture(it.texPath)
                    glActiveTexture(GL_TEXTURE0)
                    glBindTexture(GL_TEXTURE_2D, texture.id)
                    glUniform1i(glGetUniformLocation(shaderProgramId, "texSampler"), 0)

                    it.meshList.forEach{ mesh ->
                        glBindVertexArray(mesh.vaoId)
                        glUniformMatrix4fv(glGetUniformLocation(shaderProgramId, "model"), false, Matrix4f().identity().scale(0.5f).get(FloatArray(16)))
                        glDrawElements(GL_TRIANGLES, mesh.numVertices, GL_UNSIGNED_INT, 0)
                    }

                }

                glUseProgram(lightingSourceShaderProgramId)
                glBindVertexArray(lightSource3DVaoId)
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                pointLights.forEach {
                    GL20.glUniformMatrix4fv(
                        glGetUniformLocation(lightingSourceShaderProgramId, "model"),
                        false,
                        Matrix4f().identity().translate(it).scale(0.2f).get(FloatArray(16))
                    )

                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }
                glBindVertexArray(0)
                glUseProgram(0)


                camera.update()
            }
        )
    }

}

fun processTexture(texture: AITexture): Int{
    val texID: Int = glGenTextures()

    glBindTexture(GL_TEXTURE_2D, texID)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

    val width = createIntBuffer(1)
    val height = createIntBuffer(1)
    val channels = createIntBuffer(1)
    val image = stbi_load_from_memory(texture.pcDataCompressed(), width, height, channels, 0)

    println("width:${width.get()} | height:${height.get()}")

    if (image != null) {
        val format = when(channels.get(0)){
            1 -> GL_COLOR_INDEX
            2 -> GL_ALPHA
            3 -> GL_RGB
            4 -> GL_RGBA
            else -> throw RuntimeException("Error: (Texture) unknown number of channels '${channels.get(0)}'")
        }
        glTexImage2D(GL_TEXTURE_2D, 0, format, width.get(0), height.get(0), 0, format, GL_UNSIGNED_BYTE, image)
    } else{
        throw RuntimeException("Error: (Texture) could not load image '${texture.mFilename().dataString()}'")
    }

    STBImage.stbi_image_free(image)

    return texID
}
