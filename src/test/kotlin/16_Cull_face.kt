import engine.Game
import engine.GameEntity
import engine.loop
import engine.shaders.ShaderProgram
import engine.shaders.ShadersLoader
import org.joml.Vector3f
import org.junit.Test
import org.lwjgl.opengl.GL30.*
import vao.Textured3DCube
import vao.TexturedPlate

class `16_Cull_face` {

    @Test
    fun cullFace() {
        Game().start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0011f
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f, 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.501f, 2.5); it.updateModelMatrix() }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val texturedPlateVaoId = TexturedPlate().vaoId
            val plateEntity = GameEntity(texturedPlateVaoId).also { it.scale = 6; it.setPosition(0, 0, 2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val grassEntity1 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(1,0.5,3)
            val grassEntity2 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(1,0.4,0)
            val grassEntity3 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(2,0.5,3)
            val grassTexture = Texture2D("res/textures/grass.png")

            val windowEntity1 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(-0.3,0.5,0.5)
                .also {it.setRotation(1, 0, 0, 90); it.updateModelMatrix()}
            val windowEntity2 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(0.2,0.6,1.7)
                .also {it.setRotation(1, 0, 0, 90); it.updateModelMatrix()}
            val windowEntity3 = GameEntity(texturedPlateVaoId).makePlateVertical().withPosition(1.1,0.5,4.1)
                .also {it.setRotation(1, 0, 0, 90); it.updateModelMatrix()}
            val windowTexture = Texture2D("res/textures/window.png")

            val windows = arrayListOf(windowEntity1, windowEntity2, windowEntity3)

            val shader = ShadersLoader.load("15_Transparency.glsl")

            fun ShaderProgram.drawEntity(entity: GameEntity, texId: Int) {
                glBindVertexArray(entity.vaoId)
                this.setUniform("model", entity.modelMatrix)
                this.setTextureUniform("texture1", texId)
                glDrawArrays(GL_TRIANGLES, 0, 36)
            }

            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

            glFrontFace(GL_CCW)

            loop {
                shader.use {
                    it.setSceneUniforms(scene)
                    glEnable(GL_CULL_FACE)


                    it.drawEntity(cubeEntity1, cubeTexture.texID)
                    it.drawEntity(cubeEntity2, cubeTexture.texID)

                    it.drawEntity(grassEntity1, grassTexture.texID)
                    it.drawEntity(grassEntity2, grassTexture.texID)
                    it.drawEntity(grassEntity3, grassTexture.texID)

                    glDisable(GL_CULL_FACE)

                    it.drawEntity(plateEntity, plateTexture.texID)

                    windows.sortedByDescending { window ->
                        Vector3f((scene.camera as FpsCamera).cameraPos).sub(window.position).length()
                    }.forEach{ window ->
                        it.drawEntity(window, windowTexture.texID)
                    }
                }
            }
        }
    }
}
