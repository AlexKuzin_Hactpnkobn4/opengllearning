import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*

class `03_Texture_rectangle` {

    @Test
    fun run() {
        var shaderProgramId = -1
        var vaoId = -1
        var tex1 = -1
        var tex2 = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0f, 0f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   2f, 0f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   2f, 2f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0f, 2f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/03_Texture_rectangle.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                tex1 = Texture2D("res/textures/squaresProspect.jpg").texID
                glActiveTexture(GL_TEXTURE1)
                tex2 = Texture2D("res/textures/brickwall.jpg").texID
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)
            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, tex1)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, tex2)


                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

    @Test
    fun exercise1() {
        var shaderProgramId = -1
        var vaoId = -1
        var tex1 = -1
        var tex2 = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0f, 0f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   2f, 0f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   2f, 2f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0f, 2f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/03_Texture_rectangle_exercise1.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                tex1 = Texture2D("res/textures/arrow.png").texID
                glActiveTexture(GL_TEXTURE1)
                tex2 = Texture2D("res/textures/brickwall.jpg").texID
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)
            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, tex1)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, tex2)


                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

    @Test
    fun exercise2() {
        var shaderProgramId = -1
        var vaoId = -1
        var tex1 = -1
        var tex2 = -1
        var mixValue = 0.5f

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0f, 0f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   2f, 0f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   2f, 2f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0f, 2f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/03_Texture_rectangle_exercise2.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                tex1 = Texture2D("res/textures/arrow.png").texID
                glActiveTexture(GL_TEXTURE1)
                tex2 = Texture2D("res/textures/brickwall.jpg").texID
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)
            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                val step = 0.001f
                if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_UP)) {
                    mixValue = Math.max(0f, mixValue - step)
                    println("UP")
                }
                if(KeyListener.isKeyPressed(GLFW.GLFW_KEY_DOWN)) mixValue = Math.min(1f, mixValue + step)

                glUniform1i(glGetUniformLocation(shaderProgramId, "texture1"), 0)
                glUniform1i(glGetUniformLocation(shaderProgramId, "texture2"), 1)
                glUniform1f(glGetUniformLocation(shaderProgramId, "mixValue"), mixValue)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, tex1)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, tex2)


                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

}