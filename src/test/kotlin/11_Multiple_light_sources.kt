import vao.Box3D
import vao.LightSourceCube
import Window.window
import org.joml.Math.cos
import org.joml.Math.toRadians
import org.joml.Matrix4f
import org.joml.Vector3f
import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import kotlin.random.Random

class `11_Multiple_light_sources` {

    @Test
    fun multipleLightSources(){
        var box3DShaderProgramId = -1
        var lightingSourceShaderProgramId = -1

        var cubes3DVaoId = -1
        var lightSource3DVaoId = -1

        var boxTextureId = -1
        var boxReflectionTextureId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*9,(Random.nextFloat()-0.5f)*6+6,(Random.nextFloat()-0.5f)*9-5))}
            .map{ it to 0.02f + Random.nextFloat()/22}
            .toMutableList()

        val dirLightDirection = Vector3f(-2f, 10f, -2f)

        val pointLights = (1..4)
            .map { Vector3f((Random.nextFloat()-0.5f)*18,(Random.nextFloat()-0.5f)*6,(Random.nextFloat()-0.5f)*18-5) }

        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)
                glClearColor(0.1f, 0.1f, 0.1f, 1f)

                cubes3DVaoId = Box3D().vaoId
                lightSource3DVaoId = LightSourceCube().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/11_Multiple_light_sources.glsl").shaderProgramId
                lightingSourceShaderProgramId = ShaderFile("res/shaders/07_Colors_and_lighting_source.glsl").shaderProgramId

                glActiveTexture(GL_TEXTURE0)
                boxTextureId = Texture2D("res/textures/boxSide.png").texID
                glActiveTexture(GL_TEXTURE1)
                boxReflectionTextureId = Texture2D("res/textures/boxSideReflectionMap.png").texID
            },
            loop = {
                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)

                updateDirLightUniforms(box3DShaderProgramId, dirLightDirection)
                updateFlashLightUniforms(box3DShaderProgramId, camera.cameraPos, camera.cameraFront)
                pointLights.forEachIndexed { index, position ->
                    updatePointLightUniforms(box3DShaderProgramId, index, position)
                }

                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 0)
                glUniform1i(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 1)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),32f)
                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, boxTextureId)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, boxReflectionTextureId)

                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, pair.first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                glUseProgram(lightingSourceShaderProgramId)
                glBindVertexArray(lightSource3DVaoId)
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                pointLights.forEach {
                    GL20.glUniformMatrix4fv(
                        glGetUniformLocation(lightingSourceShaderProgramId, "model"),
                        false,
                        Matrix4f().identity().translate(it).scale(0.2f).get(FloatArray(16))
                    )

                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }
                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }

    fun updateDirLightUniforms(shaderProgramId: Int, direction: Vector3f){
        glUniform3f(glGetUniformLocation(shaderProgramId, "directionalLight.direction"), direction.x, direction.y, direction.z)
        glUniform3f(glGetUniformLocation(shaderProgramId, "directionalLight.ambient"), 0.2f, 0.2f, 0.2f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "directionalLight.diffuse"), 0.6f, 0.6f, 0.6f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "directionalLight.specular"), 0.5f, 0.5f, 0.5f)
    }

    fun updateFlashLightUniforms(shaderProgramId: Int, cameraPos: Vector3f, cameraFront: Vector3f){
        glUniform3f(glGetUniformLocation(shaderProgramId, "flashLight.position"), cameraPos.x, cameraPos.y, cameraPos.z)
        glUniform3f(glGetUniformLocation(shaderProgramId, "flashLight.direction"), cameraFront.x, cameraFront.y, cameraFront.z)
        glUniform1f(glGetUniformLocation(shaderProgramId, "flashLight.cutoff"), cos(toRadians(12.5f)))
        glUniform1f(glGetUniformLocation(shaderProgramId, "flashLight.outerCutoff"), cos(toRadians(17.5f)))
        glUniform1f(glGetUniformLocation(shaderProgramId, "flashLight.constant"),1f)
        glUniform1f(glGetUniformLocation(shaderProgramId, "flashLight.linear"), 0.03f)
        glUniform1f(glGetUniformLocation(shaderProgramId, "flashLight.quadratic"), 0.03f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "flashLight.ambient"), 0.2f, 0.2f, 0.2f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "flashLight.diffuse"), 0.7f, 0.7f, 0.7f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "flashLight.specular"), 0.9f, 0.9f, 0.9f)
    }

    private fun updatePointLightUniforms(shaderProgramId: Int, index: Int, position: Vector3f) {
        glUniform3f(glGetUniformLocation(shaderProgramId, "pointLights[$index].position"), position.x, position.y, position.z)

        glUniform3f(glGetUniformLocation(shaderProgramId, "pointLights[$index].ambient"), 0.2f, 0.2f, 0.2f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "pointLights[$index].diffuse"), 0.7f, 0.7f, 0.7f)
        glUniform3f(glGetUniformLocation(shaderProgramId, "pointLights[$index].specular"), 0.9f, 0.9f, 0.9f)

        glUniform1f(glGetUniformLocation(shaderProgramId, "pointLights[$index].constant"),1f)
        glUniform1f(glGetUniformLocation(shaderProgramId, "pointLights[$index].linear"),0.03f)
        glUniform1f(glGetUniformLocation(shaderProgramId, "pointLights[$index].quadratic"),0.07f)
    }
}
