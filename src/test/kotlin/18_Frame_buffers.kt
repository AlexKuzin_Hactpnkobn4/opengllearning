import engine.Game
import engine.GameEntity
import engine.loop
import engine.shaders.ShadersLoader
import imgui.ImGui
import imgui.flag.ImGuiWindowFlags
import org.junit.Test
import org.lwjgl.opengl.GL30.*
import org.lwjgl.system.MemoryUtil.NULL
import vao.ScreenQuadVao
import vao.VaoCache.Vao.*

class `18_Frame_buffers` {

    @Test
    fun frameBuffers() {
        Game(texturesPreload = true).start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0015f
            val cubeEntity1 = GameEntity(textured3DCube, "boxSide.png").withPosition(1.5, 0.501f, 2)
            val cubeEntity2 = GameEntity(textured3DCube, "marble.jpg").withPosition(-0.5, 0.501f, 2.5)

            val plateEntity = GameEntity(texturedPlate, "metal.jpg").withPosition(0,0,2).also { it.scale = 6; it.updateModelMatrix() }

            val quadVaoId = ScreenQuadVao().vaoId

            val shader = ShadersLoader.load("17_ImGui.glsl")
            val screenShader = ShadersLoader.load("18_Frame_buffer.glsl")

            val (frameBufferId, textureColorBuffer) = genFrameBuffer()



            loop {

                glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId)
                glEnable(GL_DEPTH_TEST)
                glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

                shader.use {
                    it.setSceneUniforms(scene)
                    it.drawEntity(cubeEntity1)
                    it.drawEntity(cubeEntity2)

                    it.drawEntity(plateEntity)
                }

                glBindFramebuffer(GL_FRAMEBUFFER, 0)
                glClear(GL_COLOR_BUFFER_BIT)

                screenShader.use {
                    glBindVertexArray(quadVaoId)
                    glBindTexture(GL_TEXTURE_2D, textureColorBuffer)
                    glDrawArrays(GL_TRIANGLES, 0, 6)
                }
            }
        }
    }

    @Test
    fun frameBuffersInvertedColors() {
        Game(texturesPreload = true).start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0015f
            val cubeEntity1 = GameEntity(textured3DCube, "boxSide.png").withPosition(1.5, 0.501f, 2)
            val cubeEntity2 = GameEntity(textured3DCube, "marble.jpg").withPosition(-0.5, 0.501f, 2.5)

            val plateEntity = GameEntity(texturedPlate, "metal.jpg").withPosition(0,0,2).also { it.scale = 6; it.updateModelMatrix() }

            val quadVaoId = ScreenQuadVao().vaoId

            val shader = ShadersLoader.load("15_Transparency.glsl")
            val screenShaderNormal = ShadersLoader.load("18_Frame_buffer.glsl")
            val screenShaderInvColors = ShadersLoader.load("18_Frame_buffer_inverted_colors.glsl")
            val screenShaderGrayShades = ShadersLoader.load("18_Frame_buffer_gray_shades.glsl")
            val screenShaderCoreEffects = ShadersLoader.load("18_Frame_buffer_core_effects.glsl")
            var screenShader = screenShaderNormal

            GuiLayer.setGuiCallback {

                    if(ImGui.begin("Choose screen buffer shader:", ImGuiWindowFlags.AlwaysAutoResize)){
                        if(ImGui.radioButton("Normal", screenShader == screenShaderNormal)){
                            screenShader = screenShaderNormal
                        }
                        if(ImGui.radioButton("Inverted colors", screenShader == screenShaderInvColors)){
                            screenShader = screenShaderInvColors
                        }
                        if(ImGui.radioButton("Gray shades", screenShader == screenShaderGrayShades)){
                            screenShader = screenShaderGrayShades
                        }
                        if(ImGui.radioButton("Core effect", screenShader == screenShaderCoreEffects)){
                            screenShader = screenShaderCoreEffects
                        }
                    }
                    ImGui.end()
            }

            val (frameBufferId, textureColorBuffer) = genFrameBuffer()

            loop {
                glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId)
                glEnable(GL_DEPTH_TEST)
                glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

                shader.use {
                    it.setSceneUniforms(scene)

                    it.drawEntity(cubeEntity1)
                    it.drawEntity(cubeEntity2)

                    it.drawEntity(plateEntity)
                }

                glBindFramebuffer(GL_FRAMEBUFFER, 0)
                glClear(GL_COLOR_BUFFER_BIT)

                screenShader.use {
                    glBindVertexArray(quadVaoId)
                    glBindTexture(GL_TEXTURE_2D, textureColorBuffer)
                    glDrawArrays(GL_TRIANGLES, 0, 6)
                }
            }
        }
    }

    private fun genFrameBuffer(): Pair<Int, Int> {
        val frameBufferId = glGenFramebuffers()
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId)
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f)

        val textureColorBuffer = genFrameBufferTexture()
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorBuffer, 0)


        val rbo = glGenRenderbuffers()
        glBindRenderbuffer(GL_RENDERBUFFER, rbo)
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, Window.WIDTH, Window.HEIGHT)
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo)

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
            throw RuntimeException("Framebuffer is not complete! statusCode: ${glCheckFramebufferStatus(GL_FRAMEBUFFER)}")
        }
        return Pair(frameBufferId, textureColorBuffer)
    }

    private fun genFrameBufferTexture(): Int {
        val texColorBuffer = glGenTextures()
        glBindTexture(GL_TEXTURE_2D, texColorBuffer)

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Window.WIDTH, Window.HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL )

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        return texColorBuffer
    }
}
