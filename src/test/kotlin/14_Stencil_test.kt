import engine.Game
import engine.GameEntity
import engine.loop
import engine.shaders.ShadersLoader
import org.joml.Matrix4f
import org.junit.Test
import org.lwjgl.opengl.GL30.*
import vao.Textured3DCube
import vao.TexturedPlate

class `14_Stencil_test` {

    @Test
    fun stencilTest() {
        Game().start { scene ->
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f, 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.5f, 2.5); it.updateModelMatrix() }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity =
                GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0, 0, 2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("05_3D_cube.glsl")

            val delineatingShader = ShadersLoader.load("14_Stencil_test_delineating.glsl")
            glEnable(GL_STENCIL_TEST)

            loop {
                glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)

                shader.use {
                    it.setSceneUniforms(scene)

                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    glStencilMask(0x00)
                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    glStencilFunc(GL_ALWAYS, 1, 0xFF)
                    glStencilMask(0xFF)

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", cubeEntity1.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    it.setUniform("model", cubeEntity2.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }

                delineatingShader.use {
                    it.setSceneUniforms(scene)

                    glStencilMask(0x00)
                    glStencilFunc(GL_NOTEQUAL, 1, 0xFF)
                    glDisable(GL_DEPTH_TEST)
                    ///////////////////////////////////////

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", cubeEntity1.getScaledMatrix(1.1f))
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    it.setUniform("model", cubeEntity2.getScaledMatrix(1.1f))
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    ///////////////////////////////////////
                    glStencilMask(0xFF)
                    glStencilFunc(GL_ALWAYS, 1, 0xFF)
                    glEnable(GL_DEPTH_TEST)

                }
            }
        }
    }

    @Test
    fun stencilTestSeparated() {
        Game().start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0011f

            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f, 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.5f, 2.5); it.updateModelMatrix() }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity =
                GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0, 0, 2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("05_3D_cube.glsl")

            val delineatingShader = ShadersLoader.load("14_Stencil_test_delineating.glsl")
            glEnable(GL_STENCIL_TEST)
            glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)


            fun drawBoxWithDelineating(box: GameEntity){
                shader.use(scene) {
                    it.setSceneUniforms(scene)

                    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)

                    glStencilMask(0xFF)
                    glStencilFunc(GL_ALWAYS, 1, 0xFF)

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", box.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }

                delineatingShader.use(scene) {
                    glStencilMask(2)
                    glStencilFunc(GL_NOTEQUAL, 0b00000001, 0xFF)
                    ///////////////////////////////////////

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", box.getScaledMatrix(1.1f))
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    ///////////////////////////////////////
                    glStencilMask(0xFF)
                    glStencilFunc(GL_ALWAYS, 1, 0xFF)

                }
            }
            loop {
                shader.use(scene) {
                    it.setSceneUniforms(scene)

                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    glStencilMask(0x00)
                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }


                drawBoxWithDelineating(cubeEntity1)
                drawBoxWithDelineating(cubeEntity2)
            }
        }
    }

    @Test
    fun stencilTestSeparated2() {
        Game().start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0011f
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f, 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.51f, 2.5); it.updateModelMatrix() }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity =
                GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0, 0, 2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("05_3D_cube.glsl")

            val delineatingShader = ShadersLoader.load("14_Stencil_test_delineating.glsl")
            glEnable(GL_STENCIL_TEST)


            fun drawBoxWithDelineating(box: GameEntity){
                shader.use(scene) {
                    glStencilMask(0x03)
                    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)
                    glStencilFunc(GL_ALWAYS, 1, 0xFF)

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", box.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }

                delineatingShader.use(scene) {
                    glStencilMask(0x02)
                    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)
                    glStencilFunc(GL_NOTEQUAL, 3, 0x01)

                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", box.getScaledMatrix(1.1f))
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }
                glStencilMask(0x01)
                glClear(GL_STENCIL_BUFFER_BIT)
                glStencilMask(0xFF)
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP)

            }
            loop {
                drawBoxWithDelineating(cubeEntity1)
                drawBoxWithDelineating(cubeEntity2)

                shader.use(scene) {
                    glStencilFunc(GL_NOTEQUAL, 0x02, 0xFF)
                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }
            }
        }
    }
}

fun GameEntity.getScaledMatrix(scaleMul: Number): Matrix4f {
    val currScale = this.scale
    val resultScale = currScale.toFloat() * scaleMul.toFloat()
    this.scale = resultScale
    this.updateModelMatrix()
    val result = Matrix4f(this.modelMatrix)

    this.scale = currScale
    this.updateModelMatrix()

    return result
}
