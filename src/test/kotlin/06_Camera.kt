import Window.window
import org.joml.Math.*
import org.joml.Matrix4f
import org.joml.Vector3f
import org.junit.Test
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import vao.Textured3DCube
import kotlin.random.Random

class `06_Camera` {

    @Test
    fun run() {
        var shaderProgramId = -1
        var vaoId = -1

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))
        var view: Matrix4f
        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val radius = 10f
        var cameraRotation = 0f

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*7,(Random.nextFloat()-0.5f)*4-2,(Random.nextFloat()-0.5f)*7+2))}
            .map{ it to 0.02f + Random.nextFloat()/12}
            .toMutableList()

        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)

                vaoId = Textured3DCube().vaoId

                shaderProgramId = ShaderFile("res/shaders/05_3D_cube.glsl").shaderProgramId

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                cameraRotation+=0.0005f
                val camX = sin(cameraRotation) * radius
                val camZ = cos(cameraRotation) * radius

                view = Matrix4f().lookAt(
                    Vector3f(camX,0f,camZ),
                    Vector3f(0f,0f,0f),
                    Vector3f(0f,1f,0f),
                )

                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "view"), false, view.get(FloatArray(16)))
                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "projection"), false, projection.get(FloatArray(16)))


                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "model"), false, cubeModels[index].first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

    @Test
    fun cameraMovement() {
        var shaderProgramId = -1
        var vaoId = -1

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera()

        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*7,(Random.nextFloat()-0.5f)*4-2,(Random.nextFloat()-0.5f)*7+2))}
            .map{ it to 0.02f + Random.nextFloat()/12}
            .toMutableList()

        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)

                vaoId = Textured3DCube().vaoId

                shaderProgramId = ShaderFile("res/shaders/05_3D_cube.glsl").shaderProgramId

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                camera.update()

                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "projection"), false, projection.get(FloatArray(16)))


                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "model"), false, cubeModels[index].first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }
}