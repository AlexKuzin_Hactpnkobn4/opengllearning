import org.junit.Test
import org.lwjgl.opengl.GL30.*

class `02_Texture_triangle` {

    @Test
    fun run(){
        var shaderProgramId = -1
        var vaoId = -1
        var texId = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,     1.0f, 0.0f, 0.0f,    0f, 1f,//bottom-left
            0.5f,  -0.5f,    0.0f, 1.0f, 0.0f,     1f,  1f,//bottom-right
            0f,     0.5f,    0.0f, 0.0f, 1.0f,     0.5f, 0f,//top-middle
        )

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)

                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/02_Texture.glsl").shaderProgramId
                texId = Texture2D("res/3DModels/cube/cube.png").texID
            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)
                glBindTexture(GL_TEXTURE_2D, texId)

                glDrawArrays(GL_TRIANGLES, 0, 3)

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

}