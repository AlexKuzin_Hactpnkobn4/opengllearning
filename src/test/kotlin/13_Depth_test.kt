import engine.Game
import engine.GameEntity
import engine.loop
import engine.shaders.ShadersLoader
import org.junit.Test
import org.lwjgl.opengl.GL30.*
import vao.Textured3DCube
import vao.TexturedPlate

class `13_Depth_test` {

    @Test
    fun run() {
        Game().start { scene ->
            val cubeEntity = GameEntity(Textured3DCube().vaoId)

            val cubeTexture = Texture2D("res/textures/marble.jpg")
            val shader = ShadersLoader.load("05_3D_cube.glsl")

            loop {
                shader.use {
                    it.setSceneUniforms(scene)

                    glBindVertexArray(cubeEntity.vaoId)
                    it.setUniform("model", cubeEntity.modelMatrix)
                    it.setTextureUniform("texture1", cubeTexture.texID)
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }

            }
        }
    }

    @Test
    fun depthTest() {
        Game().start { scene ->
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f , 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.5f , 2.5) ; it.updateModelMatrix()  }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity = GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0,0,2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("05_3D_cube.glsl")

            loop {
                shader.use {
                    it.setSceneUniforms(scene)
                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", cubeEntity1.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    it.setUniform("model", cubeEntity2.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }

            }
        }
    }

    @Test
    fun depthTestVisualization() {
        Game().start { scene ->
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f , 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.5f , 2.5) ; it.updateModelMatrix()  }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity = GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0,0,2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("13_Depth_test_visualization.glsl")

            loop {
                shader.use {
                    it.setSceneUniforms(scene)
                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", cubeEntity1.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    it.setUniform("model", cubeEntity2.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }

            }
        }
    }

    @Test
    fun depthTestLinearVisualization() {
        Game().start { scene ->
            val `3DCubeVao` = Textured3DCube().vaoId
            val cubeEntity1 = GameEntity(`3DCubeVao`).also { it.setPosition(1.5, 0.501f , 2); it.updateModelMatrix() }
            val cubeEntity2 = GameEntity(`3DCubeVao`).also { it.setPosition(-0.5, 0.5f , 2.5) ; it.updateModelMatrix()  }
            val cubeTexture = Texture2D("res/textures/marble.jpg")


            val plateEntity = GameEntity(TexturedPlate().vaoId).also { it.scale = 6; it.setPosition(0,0,2); it.updateModelMatrix() }
            val plateTexture = Texture2D("res/textures/metal.jpg")

            val shader = ShadersLoader.load("13_Depth_test_linear_visualization.glsl")

            loop {
                shader.use {
                    it.setSceneUniforms(scene)
                    glBindVertexArray(cubeEntity1.vaoId)
                    it.setTextureUniform("texture1", cubeTexture.texID)

                    it.setUniform("model", cubeEntity1.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    it.setUniform("model", cubeEntity2.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                    glBindVertexArray(plateEntity.vaoId)
                    it.setTextureUniform("texture1", plateTexture.texID)

                    it.setUniform("model", plateEntity.modelMatrix)
                    glDrawArrays(GL_TRIANGLES, 0, 36)

                }

            }
        }
    }

}
