import org.joml.Math.sin
import org.joml.Math.toRadians
import org.joml.Matrix4f
import org.joml.Quaternionf
import org.joml.Vector3f
import org.joml.Vector4f
import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import kotlin.math.abs
import kotlin.random.Random

class `05_Coordinates_system` {

    @Test
    fun run() {
        var shaderProgramId = -1
        var vaoId = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0.25f, 0.25f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   0.75f, 0.25f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   0.75f, 0.75f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0.25f, 0.75f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))
        val view = Matrix4f().translate(Vector3f(0f,0f,-3f))
        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/05_Coordinates_system.glsl").shaderProgramId

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)




                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "model"), false, model.get(FloatArray(16)))
                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "view"), false, view.get(FloatArray(16)))
                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "projection"), false, projection.get(FloatArray(16)))


                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

    @Test
    fun cube3D() {
        var shaderProgramId = -1
        var vaoId = -1

        val model = Matrix4f().identity()
            .rotate(toRadians(-55f), Vector3f(1f,0f, 0f))
            .scale(Vector3f(1f,1f,1f))
        val view = Matrix4f().translate(Vector3f(0f,0f,-3f))
        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)
        val cubeModels = (1..10)
            .map { (model.clone() as Matrix4f).translate(Vector3f((Random.nextFloat()-0.5f)*7,(Random.nextFloat()-0.5f)*4+6,(Random.nextFloat()-0.5f)*7-5))}
            .map{ it to 0.02f + Random.nextFloat()/12}
            .toMutableList()

        Window.run(
            init = {
                glEnable(GL_DEPTH_TEST)

                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)

                glBufferData(GL_ARRAY_BUFFER, verticesTextured3DCube.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 3, GL_FLOAT, false, 5 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 2, GL_FLOAT, false, 5 * Float.SIZE_BYTES, (3 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/05_3D_cube.glsl").shaderProgramId

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "view"), false, view.get(FloatArray(16)))
                GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "projection"), false, projection.get(FloatArray(16)))


                cubeModels.forEachIndexed{ index, pair ->
                    cubeModels[index] = pair.first.rotate(toRadians(pair.second), 1f, 0.5f, 0f) to pair.second

                    GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(shaderProgramId, "model"), false, cubeModels[index].first.get(FloatArray(16)))
                    glDrawArrays(GL_TRIANGLES, 0, 36)
                }


                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }
}