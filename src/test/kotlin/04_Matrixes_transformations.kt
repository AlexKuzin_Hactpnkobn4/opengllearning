import org.joml.Math.sin
import org.joml.Math.toRadians
import org.joml.Matrix4f
import org.joml.Quaternionf
import org.joml.Vector3f
import org.joml.Vector4f
import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import kotlin.math.abs

class `04_Matrixes_transformations` {

    @Test
    fun run() {
        var shaderProgramId = -1
        var vaoId = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0.25f, 0.25f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   0.75f, 0.25f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   0.75f, 0.75f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0.25f, 0.75f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        var transMatLocation = -1

        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/04_Matrixes_transformations.glsl").shaderProgramId
                transMatLocation = GL20.glGetUniformLocation(shaderProgramId, "transMat")

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)


                val trans = Matrix4f().identity()
                    .rotate(toRadians(90f), Vector3f(0f,0f, 1f))
                    .scale(Vector3f(0.5f,0.5f,0.5f))

                GL20.glUniformMatrix4fv(transMatLocation, false, trans.get(FloatArray(16)))


                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }

    @Test
    fun rotation(){
        var shaderProgramId = -1
        var vaoId = -1

        val vertices = floatArrayOf(
            //position      //Color                 /texCoord
            -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   0.25f, 0.25f,//bottom-left
            0.5f, -0.5f,    0.0f, 1.0f, 0.0f,   0.75f, 0.25f,//bottom-right
            0.5f, 0.5f,     0.0f, 0.0f, 1.0f,   0.75f, 0.75f,//top-middle
            -0.5f, 0.5f,    0.5f, 0.5f, 0.5f,   0.25f, 0.75f,//top-middle
        )


        val eboArray = intArrayOf(
            0, 1, 2,
            0, 2, 3,
        )

        var transMatLocation = -1
        var angle = 0f
        var angle2 = 0f


        Window.run(
            init = {
                vaoId = glGenVertexArrays()
                val vboId = glGenBuffers()
                val eboId = glGenBuffers()
                glBindVertexArray(vaoId)
                glBindBuffer(GL_ARRAY_BUFFER, vboId)


                glBufferData(GL_ARRAY_BUFFER, vertices.toBuffer(), GL_STATIC_DRAW)
                glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, 0L)
                glEnableVertexAttribArray(0)
                glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (2 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(1)
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * Float.SIZE_BYTES, (5 * Float.SIZE_BYTES).toLong())
                glEnableVertexAttribArray(2)


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboArray.toBuffer(), GL_STATIC_DRAW)

                glBindVertexArray(0)
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
                glBindBuffer(GL_ARRAY_BUFFER, 0)

                shaderProgramId = ShaderFile("res/shaders/04_Matrixes_transformations.glsl").shaderProgramId
                transMatLocation = GL20.glGetUniformLocation(shaderProgramId, "transMat")

                Texture2D("res/textures/brickwall.jpg").texID

            },
            loop = {
                glUseProgram(shaderProgramId)
                glBindVertexArray(vaoId)

                glEnableVertexAttribArray(0)
                glEnableVertexAttribArray(1)


                angle += 0.01f
                if(angle > 360f){
                    angle -= 360f
                }

                var trans = Matrix4f().identity()
                    .scale(Vector3f(0.3f,0.3f,0.3f))
                    .translate(0.4f,0.3f,0f)
                    .rotate(toRadians(angle), Vector3f(0f,0f, 1f))

                GL20.glUniformMatrix4fv(transMatLocation, false, trans.get(FloatArray(16)))

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                trans = Matrix4f().identity()
                    .translate(0.4f,0.3f,0f)
                    .scale(Vector3f(0.3f,0.3f,0.3f))
                    .rotate(toRadians(angle), Vector3f(0f,0f, 1f))

                GL20.glUniformMatrix4fv(transMatLocation, false, trans.get(FloatArray(16)))

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                angle2 += 0.0003f
                println(1 + abs(sin(angle2)))
                trans = Matrix4f().identity()
                    .translate(-0.5f,0.4f,0f)
                    .scale(Vector3f(0.5f + abs(sin(angle2))/2,0.5f + abs(sin(angle2))/2,0.3f))

                GL20.glUniformMatrix4fv(transMatLocation, false, trans.get(FloatArray(16)))

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0)

                glBindVertexArray(0)
                glUseProgram(0)
            }
        )
    }


}