import vao.LightSourceCube
import vao.Lighted3DCube
import Window.window
import org.joml.Math.sin
import org.joml.Math.toRadians
import org.joml.Matrix4f
import org.joml.Vector3f
import org.junit.Test
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL20.*
import org.lwjgl.opengl.GL30
import org.lwjgl.opengl.GL30.glBindVertexArray

class `08_Materials` {

    @Test
    fun run(){
        var box3DShaderProgramId = -1
        var lightingSourceShaderProgramId = -1
        var cubes3DVaoId = -1
        var lightSource3DVaoId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val lightPos = Vector3f(1.2f, 1.5f, 1.8f)

        var lightSourceModel: Matrix4f
        val boxModel = Matrix4f().identity()

        var color = 0f

        Window.run(
            init = {
                glEnable(GL30.GL_DEPTH_TEST)
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)

                cubes3DVaoId = Lighted3DCube().vaoId
                lightSource3DVaoId = LightSourceCube().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/08_Materials.glsl").shaderProgramId
                lightingSourceShaderProgramId = ShaderFile("res/shaders/07_Colors_and_lighting_source.glsl").shaderProgramId
            },
            loop = {
                lightPos.rotateY(toRadians(0.01f))
                lightSourceModel = Matrix4f().identity().translate(lightPos).scale(Vector3f(0.2f))

                color += 0.0001f
                val colorVector = Vector3f(
                    sin(color * 2f),
                    sin(color * 0.7f),
                    sin(color * 1.3f),
                )

                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, boxModel.get(FloatArray(16)))

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "objectColor"), colorVector.x + 0.1f, colorVector.y + 0.1f, colorVector.z + 0.1f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)


                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.position"), lightPos.x, lightPos.y, lightPos.z)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.ambient"), 0.2f, 0.2f, 0.2f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.diffuse"), 0.5f, 0.5f, 0.5f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.specular"), 1.0f, 1.0f, 1.0f)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.ambient"), 1f, 0.5f, 0.31f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 1f, 0.5f, 0.31f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 0.5f, 0.5f, 0.5f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),32f)


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                glUseProgram(lightingSourceShaderProgramId)
                glBindVertexArray(lightSource3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "model"), false, lightSourceModel.get(FloatArray(16)))

                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }

    @Test
    fun metalMaterial(){
        var box3DShaderProgramId = -1
        var lightingSourceShaderProgramId = -1
        var cubes3DVaoId = -1
        var lightSource3DVaoId = -1

        val projection = Matrix4f().perspective(toRadians(45f), 800f/600f, 0.1f, 100f)

        val camera = FpsCamera().also { it.cameraSpeed = 0.0008f }

        val lightPos = Vector3f(1.2f, 1.5f, 1.8f)

        var lightSourceModel: Matrix4f
        val boxModel = Matrix4f().identity()

        Window.run(
            init = {
                glEnable(GL30.GL_DEPTH_TEST)
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED)

                cubes3DVaoId = Lighted3DCube().vaoId
                lightSource3DVaoId = LightSourceCube().vaoId

                box3DShaderProgramId = ShaderFile("res/shaders/08_Materials.glsl").shaderProgramId
                lightingSourceShaderProgramId = ShaderFile("res/shaders/07_Colors_and_lighting_source.glsl").shaderProgramId
            },
            loop = {
                lightPos.rotateY(toRadians(0.01f))
                lightSourceModel = Matrix4f().identity().translate(lightPos).scale(Vector3f(0.2f))

                glUseProgram(box3DShaderProgramId)
                glBindVertexArray(cubes3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(box3DShaderProgramId, "model"), false, boxModel.get(FloatArray(16)))

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "objectColor"), 0.6f, 0.6f, 0.6f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "viewPos"), camera.cameraPos.x, camera.cameraPos.y, camera.cameraPos.z)


                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.position"), lightPos.x, lightPos.y, lightPos.z)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.ambient"), 0.2f, 0.2f, 0.2f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.diffuse"), 0.8f, 0.8f, 0.8f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "light.specular"), 1.0f, 1.0f, 1.0f)

                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.ambient"), 0.1f, 0.1f, 0.1f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.diffuse"), 1f, 1f, 1f)
                glUniform3f(glGetUniformLocation(box3DShaderProgramId, "material.specular"), 1f, 1f, 1f)
                glUniform1f(glGetUniformLocation(box3DShaderProgramId, "material.shininess"),200f)


                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                glUseProgram(lightingSourceShaderProgramId)
                glBindVertexArray(lightSource3DVaoId)

                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "view"), false, camera.getViewMatrix().get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "projection"), false, projection.get(FloatArray(16)))
                glUniformMatrix4fv(glGetUniformLocation(lightingSourceShaderProgramId, "model"), false, lightSourceModel.get(FloatArray(16)))

                glDrawArrays(GL_TRIANGLES, 0, 36)

                glBindVertexArray(0)
                glUseProgram(0)

                camera.update()
            }
        )
    }
}