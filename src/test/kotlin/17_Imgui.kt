import engine.Game
import engine.GameEntity
import engine.loop
import engine.shaders.ShadersLoader
import engine.shaders.UniformBuffer
import imgui.ImGui
import imgui.app.Application
import imgui.app.Application.launch
import imgui.app.Configuration
import imgui.flag.ImGuiConfigFlags
import imgui.gl3.ImGuiImplGl3
import imgui.glfw.ImGuiImplGlfw
import org.junit.Test
import org.lwjgl.glfw.GLFW
import vao.VaoCache.Vao.textured3DCube
import vao.VaoCache.Vao.texturedPlate

class `17_Imgui` {

    @Test
    fun simpleApplication(){
        class SimpleApp : Application() {
            override fun configure(config: Configuration) {
                config.title = "ImGui title!"
            }

            override fun initImGui(config: Configuration?) {
                super.initImGui(config)
                ImGui.getIO().addConfigFlags(ImGuiConfigFlags.ViewportsEnable)
            }

            override fun process() {
                ImGui.text("Hello world!")
            }
        }

        launch(SimpleApp())
    }

    @Test
    fun lowLevel() {
        Game(texturesPreload = true).start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0015f
            GLFW.glfwSetInputMode(Window.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL)

            val imGuiGl3 = ImGuiImplGl3()
            val imGuiGlfw = ImGuiImplGlfw()
            ImGui.createContext()

            imGuiGlfw.init(Window.window, true)
            imGuiGl3.init("#version 330 core")


            ImGui.styleColorsDark()
            var showText = false

            GuiLayer.setGuiCallback {
                ImGui.begin("Cool window")

                if(ImGui.button("I am a button")){
                    showText = !showText
                }

                if(showText){
                    ImGui.text("You clicked a button")
                }

                ImGui.end()
            }
            loop {
            }
        }
    }

    @Test
    fun Interface() {
        Game(texturesPreload = true).start { scene ->
            (scene.camera as FpsCamera).cameraSpeed = 0.0015f
            GLFW.glfwSetInputMode(Window.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL)


            val cubeEntity1 = GameEntity(textured3DCube, "boxSide.png").withPosition(1.5, 0.501f, 2)
            val cubeEntity2 = GameEntity(textured3DCube, "marble.jpg").withPosition(-0.5, 0.501f, 2.5)

            val plateEntity = GameEntity(texturedPlate, "metal.jpg").withPosition(0, 0, 2)
                .also { it.scale = 6; it.updateModelMatrix() }

            val shader = ShadersLoader.load("17_ImGui.glsl")
            shader.setUniformBuffer("projectionAndView", UniformBuffer.viewAndProjectionMatrices)

            var showText = false
            GuiLayer.setGuiCallback {

                if(ImGui.beginMainMenuBar()){
                    if(ImGui.beginMenu("File")){
                        ImGui.menuItem("Dummy button")
                        ImGui.endMenu()
                    }
                    ImGui.endMainMenuBar()
                }
                ImGui.begin("Cool window")

                if(ImGui.button("I am a button")){
                    showText = !showText
                }

                if(showText){
                    ImGui.text("You clicked a button")
                }

                ImGui.end()
            }
            loop {
                shader.use {
                    it.drawEntity(cubeEntity1)
                    it.drawEntity(cubeEntity2)

                    it.drawEntity(plateEntity)
                }



            }
        }
    }




}
